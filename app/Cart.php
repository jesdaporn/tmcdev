<?php
namespace App;
class Cart{

  public $items;//Array
  public $totalQuantity;//จำนวนสินค้าในตะกร้า
  public $totalPrice;//จำนวนราคารวม
  public $totalQtyOption;
  public $totalPriceOption;
  public $totalPriceItem;
  public $start_time;
  public $end_time;

  public $cart_id;

  public $max_time = 60 * 15 ;

  public $formate_start_time ;
  public $formate_end_time;
  public $unit_limit_time;

  public $totalPayment;
  public $totalAmount2c2p;
  public $delivery_fee;
  public $address;
  public $description;
  public $order_id;
  public $order_no;
  public $event_id;

  public $payment_option;

  public $provine_name;
  public $amphur_name;
  public $district_name;
  public $zipcode;

  public $ip_client;

  public $time_set;

  public function __construct($prevCart){

      $this->end_time = time();
      // ตะกร้าเก่า
      if($prevCart!=null){

         $cart_id = $prevCart->cart_id;

          $this->items=$prevCart->items;
          $this->totalQuantity=$prevCart->totalQuantity;
          $this->totalPrice=$prevCart->totalPrice;
          $this->totalPriceItem = $prevCart->totalPriceItem;
      
          $this->totalQtyOption = $prevCart->totalQtyOption;
          $this->totalPriceOption = $prevCart->totalPriceOption;

          $this->start_time = $prevCart->start_time;
          $this->end_time = $prevCart->end_time;
          $this->formate_start_time = $prevCart->formate_start_time;
          $this->formate_end_time = $prevCart->formate_end_time;

          $this->delivery_fee = $prevCart->delivery_fee;
          $this->totalPayment = $prevCart->totalPayment;
          $this->totalAmount2c2p = $prevCart->totalAmount2c2p;
          $this->address = $prevCart->address;
          $this->description = $prevCart->description;
          $this->order_id = $prevCart->order_id;
          $this->order_no = $prevCart->order_no;
          $this->event_id = $prevCart->event_id;
          $this->payment_option = $prevCart->payment_option;

          $this->provine_name = $prevCart->provine_name;
          $this->amphur_name = $prevCart->amphur_name;
          $this->district_name = $prevCart->district_name;
          $this->zipcode = $prevCart->zipcode;

          $this->ip_client = $prevCart->ip_client;
          
      }else{
        // ตะกร้าใหม่
        $this->items=[];
        $this->totalQuantity=0;
        $this->totalPrice=0;
        $this->totalPriceItem = 0;
        $this->totalQtyOption = 0;
        $this->totalPriceOption = 0;
      
      }

      $this->max_time = isset($prevCart->max_time) ? $prevCart->max_time :60*15;
      $this->unit_limit_time = ($this->max_time/60). " minute";
      $time_set=time();

  }


  public function addItem($id,$product){

        $price=(float)($product->price);

        $option_price = 0;
        $qytOption = 0;
        if ($product) {
          $qytOption = count($product->option_menu);
          foreach ($product->option_menu as $v) {

            if ($v->option_price&& $v->qty) {
              $option_price += (float) $v->option_price* (int)$v->qty;
            }
          }
        }

        if(array_key_exists($id,$this->items)){
              $productToAdd=$this->items[$id];
             //$productToAdd['quantity']++;//เพิ่มจำนวนรายการในสินค้านั้นๆ
              $productToAdd['quantity']= (int)$product->qty;
              $productToAdd['price'] =  $price;
              $productToAdd['totalSinglePrice']=$productToAdd['quantity']*$price;
        }else{
          
            $productToAdd=[
                  'quantity'=> (int)$product->qty,
                  'price'=>$price,
                  'totalSinglePrice' => (float) $product->qty*$price+$option_price,
                  'qtyOption'=> $qytOption,
                  'totalSingleOptionPrice' => $option_price,
                  'data'=>$product
            ];

        }


        $this->items[$id]=$productToAdd;
        $this->totalQuantity++;
        $this->totalPrice=$this->totalPrice+$price+ $option_price;
        $this->totalPriceItem = $this->totalPriceItem + $price ;
        $this->totalQtyOption = $this->totalQtyOption + $qytOption;
        $this->totalPriceOption = $this->totalPriceOption + $option_price;
        
  }


  public function addQuantiy($id,$product,$amount){
        if($amount>0){
          $price=(int)($product->price);
          if(array_key_exists($id,$this->items)){
                $productToAdd=$this->items[$id];
                $productToAdd['quantity']+=$amount;//เพิ่มจำนวนรายการในสินค้านั้นๆ
                $productToAdd['totalSinglePrice']=$productToAdd['quantity']*$price;
          }else{
              $productToAdd=['quantity'=>$amount,'totalSinglePrice'=>$price*$amount,'data'=>$product];
          }
        }
        $this->items[$id]=$productToAdd;
        $this->totalQuantity+=$amount;
        $this->totalPrice=$this->totalPrice+$price;
  }

  public function updatePriceQuantity(){
    
          $totalPrice=0;
          $totalQuantity=0;
          $totalPriceItem=0;
          $totalQtyOption = 0;
          $totalPriceOption = 0;

          // จำนวนสินค้าในตะกร้า
          // ราคารวม
          foreach ($this->items as $item) {
                $totalQuantity=$totalQuantity+$item['quantity'];// จำนวนสินค้ารวม
                $totalPrice=$totalPrice+$item['totalSinglePrice']; //ราคารวมของสินค้าแต่ละรายการ

                $totalPriceItem = $totalPriceItem + $item['price'];
                $totalQtyOption= $totalQtyOption + $item['qtyOption'];
                $totalPriceOption = $totalPriceOption + $item['totalSingleOptionPrice'];
               
          }
          $this->totalQuantity=$totalQuantity;
          $this->totalPrice=$totalPrice;

          $this->totalPriceItem = $totalPriceItem;
          $this->totalQtyOption = $totalQtyOption;
          $this->totalPriceOption = $totalPriceOption;
  }
  

  public function periodTime($time_set=""){
    

      date_default_timezone_set("Asia/Bangkok");

      /** time() หน่วยเป็นวินาที เริ่มจาก 01/01/1970 00:00:00 GMT */
      $time_set=$time_set?$time_set:time();

      $this->start_time= $time_set;
      $this->end_time= $time_set+$this->max_time;

      $this->formate_start_time = date("Y-m-d H:i:s", $this->start_time);
      $this->formate_end_time = date("Y-m-d H:i:s", $this->end_time);

    
    /*
     * Convert datetime to time();  
     * $date=date_create('2020-06-22 17:01:59')
      date_timestamp_get($date); 
      */
  }

  public function checkPeriod(){
      return $this->end_time >time(); 
  }


  public function getAmount2c2p($amount='00'){
     
      $arrayPrice = explode(".", $amount);

      $strprice = implode("", $arrayPrice);

      $len_price =  strlen($strprice);

      $arrayShift = array();

      if ($len_price < 12) {
        for ($i = 12; $i > $len_price; $i--) {
          array_push($arrayShift, "0");
        }
      }

      array_push($arrayShift, $strprice);

      $amount2c2p= implode("", $arrayShift);

      return $amount2c2p;

  }

}
