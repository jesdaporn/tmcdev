<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\UserRest;

class AdminRestaurantController extends Controller
{
    
    public function index(){

        $result = DB::table('ev_orders')->where('event_id', 13)->where('status', 1)->get();       

        return view('admin-restaurant.index', ['Orders'=> $result]);

    }

}
