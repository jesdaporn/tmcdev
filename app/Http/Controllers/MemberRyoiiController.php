<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\MemberManagement;


class MemberRyoiiController extends Controller
{

    public function index()
    {
        
    }

    public function register(Request $request)
    {

        $email = $request->email;
        $password = $request->password;


        $result = (object) array();
        $result->msg = (object) array();

        /** check status login */
        $cookie_member = MemberManagement::get_member_userlogin_from_cookie();
        if ($cookie_member) {
            $result->msg = "Login Success";
            $result->status = "success";
            $result->member_info = $cookie_member;
            return response()->json($result);     
            exit();
        }

        $messages = [
            'email.required' => 'กรุณาใส่ Email',
            'email.email' => 'รูปแบบอีเมล์ไม่ถูกต้อง',
            'email.unique' => 'Email นี้มีอยู่ในระบบแล้วไม่สามารถใช้ได้',
            'password.required' => 'กรุณาใส่ Password',
            'password.min' => 'รหัสผ่านต้องมีอย่างน้อย 6 ตัวอักษร',
            
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:191|string|unique:ev_member_users',
            'password' => 'required|min:6|string',
        ], $messages);


        if ($validator->fails()) {
            $result->msg = $validator->errors();
            $result->status = "error";
            return response()->json($result);
            exit();
        }

        $dataMember = (object) array();

        /** Generate Password */
        $MergePassword = MemberManagement::generate_userpwd($email, $password);

        /** เข้ารหัส Password */
        $dataMember->password  = Hash::make($MergePassword);
        $dataMember->email  =  $email;

        $dbMember = new MemberManagement();

        /** Insert Database เรียกจากclass  */
        $user_id = $dbMember->create($dataMember);

        if ($user_id) {
            $cookie_member = MemberManagement::memberlogin($email, $password);
            if ($cookie_member->status == "success") {

                $result->msg = "Register Success";
                $result->status = "success";
                $result->member_info = $cookie_member;

            } else {

                if ($cookie_member->msg_alert == "email") {
                    /**รูปแบบ message error ของ validator  return array จึงต้องจัดให้เหมือนกัน โดย Key จะเป็นชื่อ field ของการ validate ตัวนั้น */
                    $result->msg->email = array($cookie_member->msg);
                } else {
                    $result->msg->password = array($cookie_member->msg);
                }

                $result->status = "error";
            }

        } else {
            $result->msg = "Register Fail!!!";
            $result->status = "error";
        }


        return response()->json($result);
    }

    public function login(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $result = (object) array();
        $result->msg = (object) array();        


        /** check status login */
        $cookie_member = MemberManagement::get_member_userlogin_from_cookie();
        if($cookie_member){
            $result->msg = "Login Success";
            $result->status = "success";
            $result->member_info = $cookie_member;

            return response()->json($result);
            exit();
        }

        $messages = [
            'email.required' => 'กรุณาใส่ Email',
            'email.email' => 'รูปแบบอีเมล์ไม่ถูกต้อง',
            'password.required' => 'กรุณาใส่ Password',
            'password.min' => 'รหัสผ่านต้องมีอย่างน้อย 6 ตัวอักษร',
        ];

        /** validator system  */
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:191|string',
            'password' => 'required|min:6|string',
        ],$messages);

        /** check case validator */
        if ($validator->fails()) {
            $result->msg=$validator->errors();
            $result->status = "warning";
            return response()->json($result);
            exit();
        }


        /** call medthod login */
        $cookie_member = MemberManagement::memberlogin($email, $password);

        if($cookie_member->status=="success"){

            $result->msg = "Login Success";
            $result->status = "success";
            $result->member_info = $cookie_member;
  
        }else{

            if($cookie_member->msg_alert=="email"){
                /**รูปแบบ message error ของ validator  return array จึงต้องจัดให้เหมือนกัน โดย Key จะเป็นชื่อ field ของการ validate ตัวนั้น */
                $result->msg->email = array($cookie_member->msg);
            }else {
                $result->msg->password = array($cookie_member->msg);
            }
            
            $result->status = "error";
        }

        return response()->json($result);

    }
    public function socialLogin(Request $request){


        $result = (object) array();

        /** Default Message Return */
        $result->msg = "ไม่พบข้อมูลในระบบกรุณาลองใหม่";
        $result->status = "error";

        /** check status login */
        $cookie_member = MemberManagement::get_member_userlogin_from_cookie();
        if($cookie_member){
            $result->msg = "คุณได้ Login อยู่ในระบบอยู่แล้ว";
            $result->status = "success";
            $result->member_info = $cookie_member;

            return response()->json($result);
            exit();
        }

        
        if($request->profile['login_by']=="facebook"){
        
            
            $email=$request->profile['email'];
            $name=$request->profile['name'];
            $facebook_id=$request->profile['id'];
            $fb_picture=$request->profile['picture'];
            $firstname = $request->profile['first_name'];
            $lastname = $request->profile['last_name'];
            $birthday = str_replace("/", "-", $request->profile['birthday']);

            $sex = $request->profile['gender']=="male"?"1":"2";
            
            
            $member_info = DB::table('ev_member_users')->where('email', $email)->first();
            /** Check Email In Database */
            if($member_info){
                
                if(!$member_info->facebook_id){
                    /** Update facebook id */
                    $update_ufb = DB::table('ev_member_users')
                        ->where('id', $member_info->id)
                        ->update(['facebook_id' => $facebook_id, 'image_icon' => $fb_picture, 'name' => $name]);

                    if (!$update_ufb) {
                        $result->msg = "ไม่พบข้อมูลในระบบกรุณาลองใหม่";
                        $result->status = "error";

                        return response()->json($result);
                        exit();
                    }

                }

                $result = MemberManagement::facebooklogin($facebook_id, $email);
                $result->check_fb="update";
                                
            }else{

                /** create user from facebook data */
                $sql = " INSERT INTO `ev_member_users`
                        (   
                        name,         
                        email,
                        firstname,
                        lastname,
                        date_of_birth,
                        facebook_id,
                        image_icon, 
                        sex,                                                
                        created_at
                        )
                        VALUES
                        (         
                        '$name',
                        '$email',
                        '$firstname',
                        '$lastname',
                        '$birthday',
                        '$facebook_id',
                        '$fb_picture',
                        '$sex',
                        SYSDATE()
                        )";

                    if($resultInsert =  DB::insert($sql)){
                        /** call medthod login */
                         $result = MemberManagement::facebooklogin($facebook_id, $email);
                        $result->check_fb = "create";                                          
                    }else{

                        $result->msg = "ไม่พบข้อมูลในระบบกรุณาลองใหม่";
                        $result->status = "error";

                    }
    
            }

        }else{

            $result->msg = "ไม่พบข้อมูล Login กรุณาลองใหม่";
            $result->status = "warning";

        }

        return response()->json($result);

    }

    public function resetpassword(Request $request){

        $email = $request->email;

        $result = (object) array();
        $result->msg = (object) array();

        /** check status login */
        $cookie_member = MemberManagement::get_member_userlogin_from_cookie();
        if ($cookie_member) {
            $result->msg = "คุณได้ Login อยู่แล้วกรุณาไปที่ Profile เพื่อตั้งค่าใหม่";
            $result->status = "success";
            $result->member_info = $cookie_member;

            return response()->json($result);
            exit();
        }

        $messages = [
            'email.required' => 'กรุณาใส่ Email',
            'email.email' => 'รูปแบบอีเมล์ไม่ถูกต้อง',
        ];

        /** validator system  */
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:191|string',
        ], $messages);

        /** check case validator */
        if ($validator->fails()) {
            $result->msg = $validator->errors();
            $result->status = "warning";
            return response()->json($result);
            exit();
        }

        $dataUser = DB::table('ev_member_users')->where('email', $email)->first();

        if($dataUser){

            $token_remember = Str::random(60);

            /** Update To token Rember */
            $update_remember = DB::table('ev_member_users')
            ->where('id', $dataUser->id)
                ->update(['remember_token' => $token_remember, 'email_verified_at'=>date('Y-m-d H:i:s')]);

            if($update_remember){
                $result->msg = "ระบบได้ส่งลิงค์เข้าเมล์ที่แจ้งแล้ว";
                $result->status = "success";
            }else{
                $result->msg = "ไม่พบข้อมูลในระบบกรุณาตรวจสอบอีกครั้ง";
                $result->status = "error";
            }

        }else{
            $result->msg->email = array("ไม่พบอีเมล์ในระบบที่แจ้ง");
            $result->status = "error";
        }
        
       

        return response()->json($result);


    }

    public function logout(){

        //============= Logout ===========
        MemberManagement::memberlogout();

        return redirect('/');

    }


    public function editProfile($id)
    {
        



    }



    function resetEmail($email){



    }

}
