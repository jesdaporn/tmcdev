<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Mail\MailOrder;

use Illuminate\Support\Facades\Mail;


use App\Cart;
use stdClass;
use App\Orders;
use App\Consts;

use App\MemberManagement;

class RyoiiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(){


        
        /*$db_orders = new Orders();

        $result= $db_orders->getLastEvent();
  
             

        return view('index',['events'=> $result]);*/
        

        return redirect('/post/oranjiixryoiireview');
    }

    public function post($url)
    {
      
        $db_orders = new Orders();
         $event = $db_orders->getEventDetial($url);        
       
        if($event){

            $date_now = date('Y-m-d');

            $view_day_cnt = $event->view_day_cnt? $event->view_day_cnt:0;
            $view_cnt = $event->view_cnt ? $event->view_cnt : 0;
            $view_day_date = $event->view_day_date? $event->view_day_date: $date_now;

            $view_day_cnt++;
            $view_cnt++;
            if($view_day_date!= $date_now){
                $view_day_date=$date_now;
                $view_day_cnt = 1;
            }
                        

            DB::update("update ev_event set view_cnt = " . $view_cnt . ", view_day_cnt = ". $view_day_cnt . ",view_day_date='" . $view_day_date . "' where event_id = " . $event->event_id);
            
            return view('ryoii.post', ['event' => $event]);

        }else{            
            return view('home');
        }
     
        
    }


    public function addProductToCart(Request $request)
    {
       
        //============ clear session cart =======//
        //Session::forget("cart");
        
        //dump($request->all());
       // exit();

       /** เช็ครายการที่ส่งมาทำรายการ */
        if(!$request->menu_id&&!$request->event_id){            
            return redirect('/')->with('msg', 'ไม่พบรายการที่สั่งซื้อ');
        }

        /** Loop หาว่ามีรายการสินค้าที่ต้องการทำหรือไม */
        $check_item=false;
        foreach ($request->menu_id as $k => $v) {
            $qty_menu_id = "qty_menu_id_" . $v;
            if ($request->$qty_menu_id) {
                $check_item=true;
            }
        }

        /** check รายการที่สั่งซื้อ */
        if($check_item===false){
            return redirect('/')->with('msg', 'ไม่พบรายการที่สั่งซื้อ');
        }

        /** ประการตัวแปรเข้าใช้ class orders จัดการรายการสั่ง */
        $db_orders = new Orders();

        $menu_selected = array();

        /** เรียกใช้คลาส cart เพิ่มลง session */
        //$prevCart = $request->session()->get('cart');
        $prevCart=null;

        $cart = new Cart($prevCart);

        $cart->event_id = $request->event_id;            

        /** Loop หารายการที่เลือก  */
        foreach($request->menu_id as $k=>$v){

            $item = new stdClass();
            $menu_id = $v;
            $price_menu_id = "price_menu_id_".$v;
            $qty_menu_id = "qty_menu_id_" . $v;

            /** ถ้ามี qty ก็คือมีการเลือกเมนูนั้น */
            if($request->$qty_menu_id){

                $item->menu_id = $menu_id;
                $item->qty = $request->$qty_menu_id;
                $item->price = $request->$price_menu_id;
                                
               
                /** ลูปเช็คว่า option_menu เลือกมาเป็นของ menuนี้ไหม */
                $arr_option_menu = array();
               /* if ($request->option_menu) {
                    foreach ($request->option_menu as $k1 => $v1) {                                                
                        $option_menu = DB::table('ev_set_menu_options')->where('option_id', $v1)->where('set_menu_id', $v)->first();                        
                        if ($option_menu) {                           
                           // array_push($arr_option_menu,$option_menu);                            
                        }
                    }
                }*/

                $optios_menu = DB::table('ev_set_menu_options')->where('set_menu_id', $menu_id)->where('event_id', $request->event_id)->get();

                if($optios_menu){
                    foreach($optios_menu as $k2=>$v2){
                        $menu_option_inp = 'num_option_'.$v2->option_id;
                        if(isset($request->all()[$menu_option_inp])&& $request->all()[$menu_option_inp]){

                            $v2->qty = $request->all()[$menu_option_inp];
                            
                            array_push($arr_option_menu, $v2);        
                        }
                    }
                }
                

                $item->option_menu = $arr_option_menu;
        

                $cart->addItem($menu_id, $item);

                /** Update Price QTY Item */
                $cart->updatePriceQuantity();

                /*======== Set Period Time =====*/
                $cart->periodTime();
               
                array_push($menu_selected, $item);
            }            
        }

        //dump($cart);exit();
        /*========== เก็บ cookie เวลาทำการ */
        setcookie('datetime', date('Y-m-d H:i:s'), time() + 3600, '/');

        /*========== เก็บ cookie ip client address */
        $ip_clien='';
        if($request->ip()){
            $ip_clien=$request->ip();
            setcookie('ip', $ip_clien, time()+3600, '/');
        }

        $cart->ip_client = $ip_clien;
        $cart->status = 1;       

        /** Transaction Type Reserve */
        $reserve_type= Consts::$Trans_Stock_Type_Aavailable;

        $numStock = $db_orders->countStockEvent($cart->event_id);

        /** Create Cart Order */
        if(count($cart->items)>0){

            /*if($cart->totalQuantity>$numStock){
                alertUrl('รายการนี้มียอดใช้งานครบตามที่กำหนดแล้วขอบคุณที่สนใจใช้บริการ','/');
                exit();
            }*/

            /************************************************************************* */
            /*============ Insert cart order To database AND Return cart_id ===========*/
            /************************************************************************* */
            $cart_id =$db_orders->addCartOrder($cart);
                   
            if($cart_id){

                /**========== Set Cookie cart_id */
                setcookie('cart_id', $cart_id, time() + 3600, '/');                
    
                /** Loop หารายการที่สั่งเพื่อบันทึกลงใน database */
                foreach($cart->items as $item){
                
                    $set_menu_dtl = (object) array();
                    $set_menu_dtl->cart_id = $cart_id;
                    $set_menu_dtl->event_id = $cart->event_id;
                    $set_menu_dtl->status = 1;
                    $set_menu_dtl->set_menu_id = $item['data']->menu_id;
                    $set_menu_dtl->qty_unit = $item['data']->qty;
                    $set_menu_dtl->price_per_unit = $item['data']->price;                                                              
                   
                    /*============ Insert item detail To database ===========*/
                    $cartDtlId = $db_orders->addCartOrderDetail($set_menu_dtl);

                    /** Insert Option Menu  */
                    if($item['data']->option_menu){
                        
                        foreach($item['data']->option_menu as $val){
                           
                            $val->cart_id = $cart_id;
                            $val->status = 1;
                            $val->cart_dtl_id = $cartDtlId;
                            $val->price_per_unit = $val->option_price;
                            $val->qty_unit = $val->qty;
                                                       
                            $resultOption = $db_orders->addCartOptionOrder($val);                            
                        }
                        
                    }                                                                  

                }

                /** Insert to stock transaction */
                $trans_stock = (object)array();

                $trans_stock->event_id = $cart->event_id;
                $trans_stock->order_id = $cart_id;
                $trans_stock->trans_type = $reserve_type;
                $trans_stock->trans_desc = "add to cart";
                $trans_stock->qty = $cart->totalQuantity;
                $trans_stock->member_id = 0;

                $result_trans = $db_orders->stock_transaction($trans_stock);

                /** Update Stock In Event */
                $event_stock = (object) array();
                $event_stock->event_id = $cart->event_id;
                $event_stock->case_action = "reserve";
                $event_stock->qty = $cart->totalQuantity;

                $result_stock =$db_orders->UpdateStockEvent($event_stock);
                      
                return redirect('/confirm-order/'. $cart_id);

            }else{
                return redirect('/cancel-order'); 
            }            

        }else{
            return redirect('/cancel-order');            
        }

        
    }

    public function confirmorder($cart_id)
    {
        /**
         * Method Show Page View Confirm Order Form Input Address And Delivery 
         */       

        $cookie_cart= $_COOKIE;

        if(!$cart_id){
            alertloactionBack("ไม่พบรายการที่ต้องการกรุณาลองใหม่");
        }

        $db_orders = new Orders();


        $cartData = $db_orders->getCart($cart_id);

        
        if($cartData){

            /** คืนค่าระยะเวลาสร้างเป็น timestamp */
            $cartData->start_time = returnTimestamp($cartData->create_dtm);
            /** คืนค่าระยะเวลาหมดอายุเป็น timestamp โดยบวกจำนวนวินาทีของ 15 นาที */
            $cartData->end_time = returnTimestamp($cartData->create_dtm)+900;
            /**เช็คระยะเวลาหมดอายุโดยเทียบกับเวลาปัจจุบัน */
            $cartData->checkPeriod = checkPeriod($cartData->create_dtm);


            if($cartData->checkPeriod){
                return view('ryoii.confirm-order', ['cartItems' => $cartData]);
            }else{

                $db_orders->updateCartTimeOut($cart_id);

                alertUrl('รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง','/');
            }

            
        }else{
            return redirect('/')->with('msg', 'ไม่พบรายการที่สั่งซื้อ');
        }
        
    }    

    public function orderpayment(Request $request)
    {       
       
        /** เรียกใช้ class order */
        $db_orders = new Orders();

        /** เช็คว่ามีการเลือกยอมรับข้อตกลงหรือไหม */
        if(!$request->accept_terms){
            alertloactionBack("ไม่พบว่ากดยอมรับข้อตกลงกรุณาตรวจสอบอีกครั้ง");
        }



        if (!$request->cart_id) {
            alertloactionBack("ไม่พบรายการที่ต้องการกรุณาลองใหม่");
        }

        $db_orders = new Orders();

        $cart_id = $request->cart_id;

        $cartData = $db_orders->getCart($cart_id);



        $messages = [
            'address_gps.required' => 'ป้อนตำแหน่งเพื่อระบุที่หมายการจัดส่ง',
            'latitude_delivery.required' => 'ป้อนตำแหน่งเพื่อระบุที่หมายการจัดส่ง',
            'longitude_delivery.required' => 'ป้อนตำแหน่งเพื่อระบุที่หมายการจัดส่ง',
        ];

        //validate
        $request->validate([
            'address_gps' => 'required'
        ], $messages);

        if ($cartData) {

            /** คืนค่าระยะเวลาสร้างเป็น timestamp */
            $cartData->start_time = returnTimestamp($cartData->create_dtm);
            /** คืนค่าระยะเวลาหมดอายุเป็น timestamp โดยบวกจำนวนวินาทีของ 15 นาที */
            $cartData->end_time = returnTimestamp($cartData->create_dtm) + 900;
            /**เช็คระยะเวลาหมดอายุโดยเทียบกับเวลาปัจจุบัน */
            $cartData->checkPeriod = checkPeriod($cartData->create_dtm);

            $cartData->formate_end_time = date("Y-m-d H:i:s", $cartData->end_time);
        

            if ($cartData->checkPeriod) {
                
                /** Gennerate Order No. */
                $order_no = $db_orders->generateOrderNo($cart_id);

                //dump($order_no);
                //dump($cartData);
                //dump($request->all());                     


                /** Array Object สำหรับเก็บข้อมูลการจัดส่ง */
                $address_delivery = (object) array();

                /** สำหรับเก็บไว้อัพเดท  Order */
                $objOrder =  (object)array();


                $user_member = (object) array();
                /** Check insert Member  */
                $user_member->username = $request->inp_email;
                $user_member->name = $request->inp_firstname." ". $request->inp_lastname;
                $user_member->email = $request->inp_email;
                $user_member->password =Str::random(40);

                //$memberID= $db_orders->checkInsertMember($user_member);
         
                /** เพิ่มที่อยู่ในการจัดส่ง */
                $address_delivery->member_id = $request->member_id ? $request->member_id : 0;
                $address_delivery->firstname = $request->inp_firstname;
                $address_delivery->lastname = $request->inp_lastname;
                $address_delivery->email = $request->inp_email;
                $address_delivery->tel = $request->inp_tel;
                $address_delivery->address = $request->inp_address;
                $address_delivery->province_id = $request->select_province_id;
                $address_delivery->amphur_id = $request->select_amphur_id;
                $address_delivery->district_id = $request->select_district_id;
                $address_delivery->zipcode = $request->inpzip_code_id;
                $address_delivery->address_detail = $request->inp_detail_address;

                /** Insert ที่อยู่จัดส่งลง Database  */
                $id_address =  $db_orders->insertDeliveryAddress($address_delivery);

                /** ดึงชื่อ จังหวัด อำเภอ ตำบล */
                $province_name = DB::table('ar_province')->where('province_id', $request->select_province_id)->value('province_name');
                $amphur_name   = DB::table('ar_amphur')->where('amphur_id', $request->select_amphur_id)->value('amphur_name');
                $district_name = DB::table('ar_district')->where('district_id', $request->select_district_id)->value('district_name');

                $address_delivery->province_name = $province_name;
                $address_delivery->amphur_name = $amphur_name;
                $address_delivery->district_name = $district_name;

                /*dump($id_address);*/
                /*dump($address_delivery);*/
             
                if (!$id_address) {
          
                    /** Case Insert Data Address Error */

                    $db_orders->updateCartTimeOut($cart_id,'Insert Address Delivery Error');

                    alertUrl('เกิดข้อผิดพลาดระหว่างการทำรายการ กรุณาลับไปทำรายการใหม่ได้อีกครั้ง', '/');
                    exit();
                }


                /** ราคาสินสินค้าทั้งหมดและค่าจัดส่ง */
                $id_address_delivery=$id_address;
                $latitude = $request->latitude_delivery;
                $longitude = $request->longitude_delivery;
                $distance_delivery = $request->route_delivery_map;
                $amount = $cartData->amount;
                $delivery_fee = $request->delivery_fee;

                $request_tax="";
                $company_name = "";
                $company_address = "";
                $company_tax = "";
                $company_branch = "";
                

                /** เช็คค่าขอใบกำกับภาษี */
                if ($request->order_request_tax) {
                    $request_tax = $request->order_request_tax? $request->order_request_tax:0;
                    $company_name = $request->companyName? $request->companyName:"";
                    $company_address = $request->CompanyAddress? $request->CompanyAddress:"";
                    $company_tax = $request->CompanyTax? $request->CompanyTax:"";
                    $company_branch = $request->idBranch?$request->idBranch:"";
                }


                /** แปลงเป็นจำนวนตัวเลขยอดเงินสุทธิ์ตามแบบของ 2c2p */
                $totalPrice = $amount + $delivery_fee;
                $paymentMount = getAmount2c2p($totalPrice);


                $objOrder->cart_id = $cartData->cart_id;
                $objOrder->event_id = $cartData->event_id;
                $objOrder->order_no = $order_no;
                $objOrder->order_date = $cartData->order_date;
                $objOrder->order_time = $cartData->order_time;
                $objOrder->create_dtm = $cartData->create_dtm;

                $objOrder->id_address_delivery = $id_address_delivery;
                $objOrder->amount =$amount;
                $objOrder->delivery_fee = $delivery_fee;
                $objOrder->payment_amount = $paymentMount;
                $objOrder->latitude = $latitude;
                $objOrder->longitude = $longitude;
                $objOrder->distance_delivery = $distance_delivery;


                $objOrder->request_tax = $request_tax;
                $objOrder->company_name = $company_name;
                $objOrder->company_address = $company_address;
                $objOrder->company_tax = $company_tax;
                $objOrder->company_branch = $company_branch;

                $objOrder->email = $request->inp_email;
                $objOrder->address_gps = $request->address_gps;

                /** อย่าลืมเช็คค่าใช้จ่ายทั้งหมดว่าตรงกับรายการที่สั่งไหม */


                /** Insert Cart To Order */
          
                /*dump($objOrder);
                exit();*/               

                $result_order = $db_orders->insertOrder($objOrder);

                if (!$result_order) {
                    
                    alertUrl('เกิดข้อผิดพลาดระหว่างทำรายการกรุณาลองใหม่','/');
                    exit();
                }

                return redirect('/show-payment-gateway/' . $result_order);
               
            } else {

                $db_orders->updateCartTimeOut($cart_id);

                alertUrl('รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง', '/');
            }
        } else {
            return redirect('/')->with('msg', 'ไม่พบรายการที่สั่งซื้อ');
        }
        
    }

    public function showPaymentGayway($order_id){
       
        /** เรียกใช้ class order */
        $db_orders = new Orders();


        $dataOrder = $db_orders->getOrder($order_id);
       
        if ($dataOrder) {

            /** คืนค่าระยะเวลาสร้างเป็น timestamp */
            $dataOrder->start_time = returnTimestamp($dataOrder->create_dtm);
            /** คืนค่าระยะเวลาหมดอายุเป็น timestamp โดยบวกจำนวนวินาทีของ 15 นาที */
            $dataOrder->end_time = returnTimestamp($dataOrder->create_dtm) + 900;
            /**เช็คระยะเวลาหมดอายุโดยเทียบกับเวลาปัจจุบัน */
            $dataOrder->checkPeriod = checkPeriod($dataOrder->create_dtm);

            $dataOrder->formate_end_time = date("Y-m-d H:i:s", $dataOrder->end_time);
 

            if ($dataOrder->checkPeriod) {


                /** ดึงชื่อ จังหวัด อำเภอ ตำบล */
                $province_name = DB::table('ar_province')->where('province_id', $dataOrder->address_delivery->province_id)->value('province_name');
                $amphur_name   = DB::table('ar_amphur')->where('amphur_id', $dataOrder->address_delivery->amphur_id)->value('amphur_name');
                $district_name = DB::table('ar_district')->where('district_id', $dataOrder->address_delivery->district_id)->value('district_name');

                $dataOrder->address_delivery->province_name = $province_name;
                $dataOrder->address_delivery->amphur_name = $amphur_name;
                $dataOrder->address_delivery->district_name = $district_name;

                return view('ryoii.order-payment',['OrderItems'=>$dataOrder]);
            } else {

                $db_orders->updateCartTimeOut($order_id);

                alertUrl('รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง', '/');
            }

   
        }else{
            return redirect('/')->with('msg', 'ไม่พบรายการที่สั่งซื้อ');
        }
    }


    public function cancelOrder()
    {


            /** get cart in session update status cancel order */
            $cart = Session::get('cart');

            if ($cart && $cart->order_id) {
                $datatime = date('Y-m-d H:i:s');
                DB::table('ev_carts')
                ->where('cart_id', $cart->order_id)
                    ->update(['status' => 0, 'update_dtm' => $datatime, 'cancel_dtm' => $datatime]);
            }

            /**============ Cancel session cart =======*/
            Session::forget("cart");

            /*========= Function Echo Javascript Alert And Redirect URL ==========*/
            alertUrl("ไม่พบรายการที่ต้องการทำ");
    }

    public function resultpayment(Request  $request)
    {

        //$response = file_get_contents('php://input');
        //echo "Response:<br/><textarea style='width:100%;height:80px'>" . $response . "</textarea>";

        

        $payment = (object)array();
        $payment->order_no = $request->order_id;
        $payment->transaction_datetime = $request->transaction_datetime;
        $payment->amount = $request->amount;
        $payment->approval_code = $request->approval_code;

        $payment->payment_channel_code = $request->payment_channel? $request->payment_channel:"";
        $payment->payment_status_code = $request->payment_status? $request->payment_status:"";        
        $payment->channel_response_code = $request->channel_response_code? $request->channel_response_code:"";
        $payment->channel_response_desc = $request->channel_response_desc? $request->channel_response_desc:"";


        $PaymentStatus = array(
            "000" => "Payment Successful",
            "001" => "Payment Pending",
            "002" => "Payment Rejected",
            "003" => "Payment was canceled by user",
            "999" => "Payment Failed"
        );

        if($request->payment_status== "000")$payment->status_status_desc= $PaymentStatus[$request->payment_status];
        if($request->payment_status == "001") $payment->status_status_desc = $PaymentStatus[$request->payment_status];
        if ($request->payment_status == "002") $payment->status_status_desc = $PaymentStatus[$request->payment_status];
        if ($request->payment_status == "003") $payment->status_status_desc = $PaymentStatus[$request->payment_status];
        if ($request->payment_status == "999") $payment->status_status_desc = $PaymentStatus[$request->payment_status];

           
            if($payment->order_no){
                
                /*DB::enableQueryLog();*/
                


                /**เหลืออัพเดท stock ปรับว่าสินค้าคงเหลือ และเก็บ logs */

                $result = DB::table('ev_orders')->where('order_no', $payment->order_no)->update([
                    'payment_channel_code' => $payment->payment_channel_code,
                    'channel_response_code' => $payment->channel_response_code,
                    'channel_response_desc' => $payment->channel_response_desc,
                    'payment_status_code' => $payment->payment_status_code,
                    'update_dtm'=>date('Y-m-d H:i:s')
                ]);

                /*dump(DB::getQueryLog());
                dump($payment);
                dump($result);*/      
                        
            }
 

        return redirect('/');
        /*if($payment->payment_status_code){
            return view('ryoii.result-payment', ['payment' => $payment]);
        }else{
            return redirect('/');
        }*/

       
    }

    public function profile()
    {
        /** check status login */
        $cookie_member = MemberManagement::get_member_userlogin_from_cookie();
        
        if (!$cookie_member) {
            //alertloactionBack('ไม่พบข้อมูลกรุณาเข้าสู้ระบบ');
            return redirect('/');
        }

        $profile = DB::table('ev_member_users')->where('id',$cookie_member->id)->first();


        if($profile){
            return view('ryoii.profile', ['profile' => $profile]);
        }else{
            alertloactionBack('ไม่พบข้อมูลในระบบ');
        }

       
    }

    public function updateProfile(){


        $dataProfile = array();

        $id = $request->profile_id;
        $name = $request->inp_name;
        $firstname = $request->inp_firstname;
        $lastname = $request->inp_lastname;
        $tel = $request->inp_tel;
        $sex = isset($request->sex)? $request->sex:"";
        $image_icon = $request->image_icon;


        $messages = [
            'inp_name.max' => 'ชื่อ(นามสมมติ) ต้องไม่เกิน 191 ตัวอักษร',
            'inp_firstname.max' => 'ชื่อ ต้องไม่เกิน 255 ตัวอักษร',
            'inp_lastname.max' => 'นามสกุล ต้องไม่เกิน 255 ตัวอักษร',
            'inp_tel.numeric' => 'กรุณาใส่เบอร์โทรด้วยตัวเลข',
            'profile_image.max' => 'กรุณาใช้รูปไม่เกิน 5 MB.',
        ];

        //validate
        $request->validate([
            'inp_name' => 'max:191|string',
            'inp_firstname' => 'max:255|string',
            'inp_lastname' => 'max:255|string',            
            'profile_image' => 'file|image|mimes:jpeg,png,jpg|max:5120'
        ],$messages);


        if ($request->hasFile("profile_image")) {
            
            // convert image name
            $stringImageReFormat = base64_encode('_' . time());
            $ext = $request->file('profile_image')->getClientOriginalExtension();
            $imageName = $stringImageReFormat . "." . $ext;
            $imageEncoded = File::get($request->profile_image);
           
            //upload & insert
            $fileStatus = Storage::disk('local')->put('public/profile/' . $imageName, $imageEncoded);
            
            $image_icon = 'profile/' . $imageName;
        }


        
        $dataProfile['name']=$name;
        $dataProfile['firstname'] = $firstname;
        $dataProfile['lastname'] = $lastname;
        $dataProfile['tel'] = $tel;
        $dataProfile['sex'] = $sex;
        $dataProfile['image_icon'] = $image_icon;


        $update_ufb = DB::table('ev_member_users')
        ->where('id', $id)
        ->update($dataProfile);

        return redirect('/profile');

    }


    public function transactions(Request $request)
    {
        return view('ryoii.transactions');
    }


    public function trackorder(Request $request)
    {      

        $dataOrder = array();

        if($request->inp_search_order){
            $dataOrder = DB::select("select * from ev_orders where (order_no = '$request->inp_search_order' OR email = '$request->inp_search_order') AND address_gps is not null ");
            
            if($dataOrder){
                foreach($dataOrder as $k=>$v){
                    $dataOrder[$k]->order_detail = DB::table('ev_order_dtl')->join('ev_set_menu', 'ev_set_menu.set_menu_id', '=', 'ev_order_dtl.set_menu_id')
                        ->select('ev_order_dtl.*', 'ev_set_menu.menu_name_th')->where('order_id', $v->order_id)->get();

                    //$dataOrder[$k]->address_delivery = DB::table('ev_delivery_address')->where('id', $v->id_address_delivery)->first();

                    if ($dataOrder[$k]->order_detail) {
                        foreach ($dataOrder[$k]->order_detail as $k1 => $v1) {
                            $dataOrder[$k]->order_detail[$k1]->order_option = DB::table('ev_order_options')
                                ->join('ev_set_menu_options', 'ev_set_menu_options.option_id', '=', 'ev_order_options.option_id')
                                ->where(['ev_order_options.order_id' => $v1->order_id, 'ev_order_options.set_menu_id' => $v1->set_menu_id, 'ev_order_options.event_id' => $v1->event_id])->get();
                        }
                    }
                }
            }

  

            
        }

       
        
        return view('ryoii.track-order',['dataOrder'=>$dataOrder]);
    }


    public function paymentGateway(Request $request){

        $order_id = $request->order_id;

        if(!$order_id){
            alertloactionBack('ไม่พบข้อมูลการทำรายการกรุณาตวรจสอบหรือทำรายการใหม่อีกครั้ง');
        }

        if(!$request->paymentOption){
           alertloactionBack('ไม่พบว่าท่านเลือกช่องทางจ่ายเงิน กรุณาเลือกช่องทางจ่ายเงิน');
        }

        /** เรียกใช้ class order */
        $db_orders = new Orders();

     
        $dataOrder = $db_orders->getOrder($order_id);
        //dump($dataOrder);exit();
        if ($dataOrder) {
           /* if (!$dataOrder->address_delivery->email) {
                alertloactionBack('ไม่พบว่าท่านเลือกช่องทางจ่ายเงิน กรุณาเลือกช่องทางจ่ายเงิน');
            }*/
            /** คืนค่าระยะเวลาสร้างเป็น timestamp */
            $dataOrder->start_time = returnTimestamp($dataOrder->create_dtm);
            /** คืนค่าระยะเวลาหมดอายุเป็น timestamp โดยบวกจำนวนวินาทีของ 15 นาที */
            $dataOrder->end_time = returnTimestamp($dataOrder->create_dtm) + 900;
            /**เช็คระยะเวลาหมดอายุโดยเทียบกับเวลาปัจจุบัน */
            $dataOrder->checkPeriod = checkPeriod($dataOrder->create_dtm);

            $dataOrder->formate_end_time = date("Y-m-d H:i:s", $dataOrder->end_time);

            $dataOrder->amount_2c2p = getAmount2c2p(number_format($dataOrder->payment_amount,2,'.',''));


            if ($dataOrder->checkPeriod) {

                //Merchant's account information
                $merchant_id = "764764000003263";        //Demo Get MerchantID when opening account with 2C2P                                
                $secret_key = "741F87A4C3A4E5473C599CD7D818342DD340EABB887048784A3EBD85950C0E7A";    //Get SecretKey from 2C2P PGW Dashboard

                //$merchant_id = "764764000003263";
                //$secret_key = "FBE79C5F196B753C404A76CE63DB997975EF4AF9ACE57ACC54AFE37F1D62BC96";    //Get SecretKey from 2C2P PGW Dashboard


                //Transaction information
                $payment_description  = $dataOrder->event_name_th;
                $order_id  = $dataOrder->order_no;

                $currency = "764";
                //formate amout '000000052500';
                $amount  = $dataOrder->amount_2c2p;
                //$amount = '000000001000';
               

                $payment_option = $request->paymentOption;

                $customer_email = $dataOrder->address_delivery->email;

                $datetime_15 = strtotime('+15 minutes', strtotime(date('Y-m-d H:i:s')));

                $date_expiry= date('Y-m-d H:i:s', $datetime_15);

                $payment_expiry = $date_expiry;

                //Request information
                $version = "8.5";
                
                /** Demo Url Payment */
                $payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
                
                /** URL PAYMENT */
                //$payment_url = "https://t.2c2p.com/RedirectV3/payment";
                               

                $result_url_1 = config('app.url')."/result-payment";

                //Construct signature string
                $params = $version . $merchant_id . $payment_description . $order_id . $currency . $amount . $customer_email. $result_url_1 . $payment_option. $payment_expiry;
                //dump($params);
                //dump($dataOrder);
                //exit();

                $hash_value = hash_hmac('sha256', $params, $secret_key, false);    //Compute hash value

                echo '<form id="myformPayment" method="post" action="' . $payment_url . '">
                        <input type="hidden" name="version" value="' . $version . '" />
                        <input type="hidden" name="merchant_id" value="' . $merchant_id . '" />
                        <input type="hidden" name="currency" value="' . $currency . '" />
                        <input type="hidden" name="result_url_1" value="' . $result_url_1 . '" />
                        <input type="hidden" name="payment_option" id="payment_option" value="' . $payment_option . '" />
                        <input type="hidden" name="hash_value" value="' . $hash_value . '" />
                        <input type="hidden" name="payment_description" value="' . $payment_description . '" />
                        <input type="hidden" name="order_id" value="' . $order_id . '" />
                         <input type="hidden" name="customer_email" value="' . $customer_email . '" />
                        <input type="hidden" name="amount" value="' . $amount . '" />
                        <input type="hidden" name="payment_expiry" value="' . $payment_expiry . '" />
                    </form>
                <script type="text/javascript">
                    document.getElementById("myformPayment").submit();
                </script>';    

            } else {

                $db_orders->updateCartTimeOut($order_id);

                alertUrl('รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง', '/');
            }
        }else{
            alertloactionBack('ไม่พบข้อมูลการทำรายการกรุณาตวรจสอบหรือทำรายการใหม่อีกครั้ง');
        }

    }

    public function testmail()
    {

        $email= 'jackinside83@gmail.com';

        Mail::to($email)->send(new MailOrder());
    }

}
