<?php

    function alertUrl($tex,$url="/"){
        echo "<script>alert('".$tex."')</script>";
        echo "<script>window.location.href='".$url."'</script>"; 
    }


    function alertloactionBack($tex)
    {
        echo "<script>alert('" . $tex . "')</script>";
        echo "<script>window.history.go(-1);</script>";
    }


    function checkPeriod($datetimestart,$max_timestamp=900){

        /** 60*15=900 */
        
        date_default_timezone_set("Asia/Bangkok");


        $date_time=date_create($datetimestart);
        $timestart= date_timestamp_get($date_time); 

        
        $end_time = $timestart + $max_timestamp;
        
        return $end_time > time(); 

    }

    function returnTimestamp($datetime){

        date_default_timezone_set("Asia/Bangkok");

        $date_time = date_create($datetime);
        $returntimestamp = date_timestamp_get($date_time); 

        return $returntimestamp;

    }

    function getAmount2c2p($amount='00'){
     
      $arrayPrice = explode(".", $amount);


      $strprice = implode("", $arrayPrice);

      $len_price =  strlen($strprice);

      $arrayShift = array();

      if ($len_price < 12) {
        for ($i = 12; $i > $len_price; $i--) {
          array_push($arrayShift, "0");
        }
      }

      array_push($arrayShift, $strprice);

      $amount2c2p= implode("", $arrayShift);

      return $amount2c2p;

  }

  function calculateDelivery($val){

        $distance = $val;
        $price_bath = 50;
        if($distance<=3000){
            $price_bath = 0;
        }else{
            $distance = distance-3000;
            $price_bath = (distance/1000)*10;
        }

        return round($price_bath,2);

  }

?>