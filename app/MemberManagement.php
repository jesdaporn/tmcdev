<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class MemberManagement{


    private $MemberUserLogin = 'RyoiiShopMemberLoginBussiness';
    private $MemberRemember = 'RyoiiShopReMemberUserBussiness';
    private $mask_key = "adminnruob4289ryoiishopkrbussinsessprb6395#ib8fnp2vv";


    public static function generate_userpwd($uname, $upwd)
    {
        $hashuseradmin = $upwd.'base64:H/uSgEfSA6xjJGIwi+EJo6f9X1OoifmuvybHFX0BHAg='.$uname;
    
        return $hashuseradmin;
    }



    private function VerifyLogin($username, $password)
    {

        //Hash::check($username, $password);

        //fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        /*if (auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password']))) {

        }*/
    }

    public function create($val){

        $sql = " INSERT INTO `ev_member_users`
            (            
            email,
            password,
            created_at
            )
            VALUES
            (         
            '$val->email',
            '$val->password',
            SYSDATE()
            )";

        if($result =  DB::insert($sql)){
            $user_id = DB::getPDO()->lastInsertId();
        }else{
            $user_id=0;
        }
    
        return $user_id;
    }

    

    public static function get_member_userlogin_from_cookie()
    {
        $mbuser = new MemberManagement();

        if (!isset($_COOKIE[$mbuser->MemberUserLogin])) {
            //--- ถ้าไม่พบค่าใน cookie ให้ return กลับ
            return false;
        }
        $email = $_COOKIE[$mbuser->MemberUserLogin];

        $member_info = DB::table('ev_member_users')->where('email', $email)->first();

        $session = false;

        if (isset($member_info->id)) {

            $session = (object) array();
            $session->id        = $member_info->id;
            $session->username  = $member_info->username;
            $session->email     = $member_info->email;
            $session->name      = $member_info->name;
            
            setcookie($mbuser->MemberUserLogin, $email, time() + (86400 * 1), '/', '', 0);

        }

        return $session;
    }

    public static function memberlogin($email, $password)
    {
        $mbuser = new MemberManagement();

        $dataUser = DB::table('ev_member_users')->where('email', $email)->first();
        $result = (object) array();

        if ($dataUser) {

            $MergePassword = MemberManagement::generate_userpwd($email, $password);

            if (Hash::check($MergePassword, $dataUser->password)) {
                
                $result->msg = "Login Success";
                $result->status = "success";
                
                $member_info = (object)array();
                $member_info->id = $dataUser->id;
                $member_info->username = $dataUser->username;
                $member_info->name = $dataUser->name;
                $member_info->email = $dataUser->email;

                $result->member_info = $member_info;


                /** Update Last Login */
                DB::update('update ev_member_users set last_login = SYSDATE() where id = ' . $dataUser->id);

                setcookie($mbuser->MemberUserLogin, $email, time() + (86400 * 1), '/', '', 0);

            } else {

                $result->msg = "รหัสไม่ตรงกับข้อมูล";
                $result->msg_alert = "password";
                $result->status = "error";
            }

        }else{

            $result->msg = "ไม่พบข้อมูล Email นี้ในระบบ";
            $result->msg_alert = "email";
            $result->status = "error";

        }
        
        return $result;
    }

    public static function facebooklogin($fbuid, $fb_email)
    {
        $mbuser = new MemberManagement();
        $result = (object) array();

        $dataUser = DB::table('ev_member_users')->where('email', $fb_email)->where('facebook_id',$fbuid)->first();

        if($dataUser){

            $result->msg = "Login Success";
            $result->status = "success";
                
            $member_info = (object)array();
            $member_info->id = $dataUser->id;
            $member_info->username = $dataUser->username;
            $member_info->name = $dataUser->name;
            $member_info->email = $dataUser->email;

            $result->member_info = $member_info;

            /** Update Last Login */
            DB::update('update ev_member_users set last_login = SYSDATE() where id = ' . $dataUser->id);

            setcookie($mbuser->MemberUserLogin, $fb_email, time() + (86400 * 1), '/', '', 0);
            
        }else {

            $result->msg = "ไม่พบข้อมูล Login กรุณาลองใหม่";            
            $result->status = "error";
        }

        return $result;
         
    }

    public static function memberlogout()
    {

        $mbuser = new MemberManagement();

        $user_email = '';
        setcookie($mbuser->MemberUserLogin, $user_email, time() - (86400 * 1), '/', '', 0);
    }

    public static function checkMemberAuthen($member_info)
    {
        if (!isset($member_info->id)) {
            return FALSE;
        }
        /* if ($member_info->perm < 1) {
             return FALSE;
         }*/
        return TRUE;
    }


}

?>