<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Orders extends Model
{
    protected $table = 'ev_event';


    public function getLastEvent($limit=8){


        $events = DB::table('ev_event')->where('status',1)->paginate($limit);

        return $events;

    }

    public function getEventDetial($url)
    {
        
        $event = DB::table('ev_event')->where('url_rewrite', $url)->first();

        if($event){
            
            $catesetmenu = DB::table('ev_menu_category')->where('event_id', $event->event_id)->get();
            if($catesetmenu){
                foreach($catesetmenu as $k=>$v){
                    $set_menu = DB::table('ev_set_menu')->where('cate_id', $v->cate_id)->get();

                    $catesetmenu[$k]->setmenu = $set_menu;

                    if($set_menu){
                        foreach($set_menu as $k1=>$v1){
                            $option_menu = DB::table('ev_set_menu_options')->where('set_menu_id', $v1->set_menu_id)->where('status',1)->get();
                            $catesetmenu[$k]->setmenu[$k1]->option_menu = $option_menu;
                        }
                    }
                }                
            }

            $event->catesetmenu=$catesetmenu;

        }

        return $event;
    }


    public function getCart($cart_id){

        $cart = DB::table('ev_carts')
            ->join('ev_event', 'ev_event.event_id', '=', 'ev_carts.event_id')
            ->select('ev_carts.*', 'ev_event.event_name_th','ev_event.event_poster')
            ->where('ev_carts.cart_id', $cart_id)->first();

        if($cart){

            $cart->cart_detail = DB::table('ev_carts_dtl')
                                    ->join('ev_set_menu', 'ev_set_menu.set_menu_id', '=', 'ev_carts_dtl.set_menu_id')
                                    ->select('ev_carts_dtl.*', 'ev_set_menu.menu_name_th')
                                    ->where('ev_carts_dtl.cart_id', $cart_id)
                                    ->where('ev_carts_dtl.event_id', $cart->event_id)
                                    ->get();

           if($cart->cart_detail){
               foreach($cart->cart_detail as $k=>$v){
                    $cart->cart_detail[$k]->cart_option=DB::table('ev_carts_options')
                    ->join('ev_set_menu_options', 'ev_set_menu_options.option_id','=', 'ev_carts_options.option_id')
                   ->where(['ev_carts_options.cart_dtl_id'=>$v->cart_dtl_id, 'ev_carts_options.cart_id'=>$v->cart_id, 'ev_carts_options.event_id'=>$v->event_id])->get();
               }
           }                         

        }

        return $cart;
    }


    public function getCartDetail($cart_id)
    {
        $cartDetail = DB::table('ev_carts_dtl')->where('cart_id', $cart_id)->get();

        return $cartDetail;

    }

    public function getOrder($order_id)
    {

        
        $order = DB::table('ev_orders')
        ->join('ev_event', 'ev_event.event_id', '=', 'ev_orders.event_id')
        ->select('ev_orders.*', 'ev_event.event_name_th', 'ev_event.event_poster')
        ->where('ev_orders.order_id', $order_id)->first();

        if ($order) {

            $order->order_detail = DB::table('ev_order_dtl')->join('ev_set_menu', 'ev_set_menu.set_menu_id', '=', 'ev_order_dtl.set_menu_id')
                ->select('ev_order_dtl.*', 'ev_set_menu.menu_name_th')->where('order_id', $order_id)->get();
                
            $order->address_delivery = DB::table('ev_delivery_address')->where('id', $order->id_address_delivery)->first();

            if ($order->order_detail) {
                foreach ($order->order_detail as $k => $v) {
                    $order->order_detail[$k]->order_option = DB::table('ev_order_options')
                        ->join('ev_set_menu_options', 'ev_set_menu_options.option_id', '=', 'ev_order_options.option_id')
                        ->where(['ev_order_options.order_id' => $v->order_id, 'ev_order_options.set_menu_id' => $v->set_menu_id, 'ev_order_options.event_id' => $v->event_id])->get();
                }
            }     
        }
        return $order;
    }

    public function generateOrderNo($cart_id=""){

        $today  = date('Ym');
        //$order_no = $today . '-' . str_pad(mt_rand(0, 999999),6, '0', STR_PAD_LEFT) . $num;
        $order_no = $today . '-' . str_pad($cart_id, 10, '0', STR_PAD_LEFT) ;

        return $order_no;
    }


    public function addCartOrder($cart){

        $data_now = date('Y-m-d H:i:s');

        $sql = " INSERT INTO `ev_carts`
            (
            `event_id`,
            `order_date`,
            `order_time`,
            `amount`,
            `ip_client`,
            `status`,
            `create_user`,
            `create_dtm`
            )
            VALUES
            (         
            '$cart->event_id',
            CURDATE(),
            CURTIME(),
            '$cart->totalPrice',
            '$cart->ip_client',
            '$cart->status',
            '',
            '$data_now'
            )";

        $result =  DB::insert($sql);
        $cart_id = DB::getPDO()->lastInsertId();
        
        return $cart_id;

    }

    public function stock_transaction($val){


        $sql = " INSERT INTO `ev_stock_transaction`
            (
            `event_id`,
            `order_id`,
            `trans_dtm`,
            `trans_type`,
            `trans_desc`,
            `qty`,
            `member_id`)
            VALUES
            (         
            '$val->event_id',
            '$val->order_id',
            CURDATE(),
            '$val->trans_type',
            '$val->trans_desc',
            '$val->qty',
            '$val->member_id'
            )";

        $result =  DB::insert($sql);

        return $result;


    }

    public function UpdateStockEvent($val)
    {


        //============== update Stock เพื่อจองไว้ก่อน ===============
        /**
         * ถ้าเพิ่มลงตะกร้า ให้เพิ่มลงฟิล์ด reserve_stock และ ลบค่า available 
         * ถ้าขายสำหรับให้เพิ่มลง sale และ ลบค่า reserve
         * ถ้ายกเลิก ให้บวกเพิ่ม available แล้วลบค่า reserve
         * 
         */

        /** Step One Check value available_stock in Event */
        $event_stock = DB::table('ev_event')->where('event_id',$val->event_id)->first();

        if($event_stock->available_stock>$val->qty){

            $available_stock = $event_stock->available_stock;
            $reserve_stock = $event_stock->reserve_stock;
            $sale_stock = $event_stock->sale_stock;

            $result=false;

            if ($val->case_action == "reserve") {

                $available_stock= $available_stock-$val->qty;

                $reserve_stock = $reserve_stock + $val->qty;

               /* return 'update ev_event set available_stock = ' . $available_stock . ',reserve_stock=' . $reserve_stock . ' where event_id = ' . $val->event_id;
                exit();*/

                $result = DB::update('update ev_event set available_stock = '.$available_stock.',reserve_stock='. $reserve_stock.' where event_id = '.$val->event_id);

            }else if($val->case_action == "add_available"){


                $available_stock = $available_stock + $val->qty;

                $result = DB::update('update ev_event set available_stock = ' . $available_stock . ' where event_id = ' . $val->event_id);

            }else if($val->case_action == "sale"){

                $reserve_stock = $reserve_stock - $val->qty;
                if ($reserve_stock > 0) {
                    $sale_stock = $sale_stock + $val->qty;
                    $reserve_stock = $reserve_stock - $val->qty;
                    $result = DB::update('update ev_event set sale_stock = ' . $sale_stock . ',reserve_stock=' . $reserve_stock . ' where event_id = ' . $val->event_id);
                }
                
                

            }else if($val->case_action == "cancel"){

                $data_unit = DB::select('select sum(qty_unit) as qty from ev_carts_dtl where cart_id = ? and event_id = ?', [$val->cart_id,$val->event_id]);

                $qty_unit=0;
                if($data_unit){
                    $qty_unit = $data_unit[0]->qty;
                }

                if($reserve_stock>0){

                    $available_stock = $available_stock + $qty_unit;

                    $reserve_stock = $reserve_stock - $qty_unit;

                    $result = DB::update('update ev_event set available_stock = ' . $available_stock . ',reserve_stock=' . $reserve_stock . ' where event_id = ' . $val->event_id);
                }
                                
               /* return 'update ev_event set available_stock = ' . $available_stock . ',reserve_stock=' . $reserve_stock . ' where event_id = ' . $val->event_id;
                exit();*/

            
                return $result;

            }

            return $result;

        }else{
            return false;
        }

    }

    public function countStockEvent($event_id){

        $event_stock = DB::table('ev_event')->where('event_id', $event_id)->first();

        return $event_stock->available_stock? $event_stock->available_stock:0;

    }

    public function checkStockEvent($val)
    {


        //============== Check Stock Event ===============



    }

    public function updateCartOrder($val,$cart_id){


        /** Update Payment Option And Status Cart Order */

      $result = DB::table('ev_carts')
            ->where('cart_id', $cart_id)
            ->update($val);

        return $result;
    }

    function updateCartTimeOut($cart_id,$cancle_resson= "Session timeout"){


        $sql= "UPDATE ev_carts
                SET
                    status = 0,
                    cancel_reason = '$cancle_resson',                    
                    cancel_dtm = SYSDATE(),
                    update_dtm = SYSDATE()
                WHERE cart_id ='$cart_id' ";

        $result =  DB::update($sql);

        return $result;
    }


    public function addCartOrderDetail($cart)
    {

        $sql = " INSERT INTO `ev_carts_dtl`
            (
            `cart_id`,
            `event_id`,
            `set_menu_id`,
            `price_per_unit`,
            `qty_unit`,
            `status`,
            `create_user`,
            `create_dtm`
            )
            VALUES
            (         
            '$cart->cart_id',
            '$cart->event_id',
            '$cart->set_menu_id',
            '$cart->price_per_unit',
            '$cart->qty_unit',
            '$cart->status',
            '',
            SYSDATE()
            )";

        $result =  DB::insert($sql);
        $cart_dtl_id = DB::getPDO()->lastInsertId();
        return $cart_dtl_id;
    }
    
    public function addCartOptionOrder($cart){

        
        $sql = " INSERT INTO `ev_carts_options`
            (
            cart_dtl_id,
            cart_id,
            event_id,
            set_menu_id,
            option_id,
            price_per_unit,
            qty_unit,
            status,
            create_user,
            create_dtm
            )
            VALUES
            (         
            '$cart->cart_dtl_id',
            '$cart->cart_id',
            '$cart->event_id',
            '$cart->set_menu_id',
            '$cart->option_id',
            '$cart->price_per_unit',
            '$cart->qty_unit',
            '$cart->status',
            '',
            SYSDATE()
            )";

        $result =  DB::insert($sql);

        return $result;
        
    }


    public function insertOrder($val){
        
        $sql = " INSERT INTO `ev_orders`
                        (
                        `event_id`,
                        `order_date`,
                        `order_no`,
                        `order_time`,
                        `create_dtm`,
                        `id_address_delivery`,
                        `amount`,
                        `delivery_fee`,
                        `payment_amount`,
                        `latitude`,
                        `longitude`,
                        `distance_delivery`,
                        `request_tax`,
                        `company_name`,
                        `company_address`,
                        `company_tax`,
                        `company_branch`,
                        `email`,
                        `address_gps`
                        )
                    VALUES
                        (
                            '$val->event_id',
                            '$val->order_date',
                            '$val->order_no',
                            '$val->order_time',
                            '$val->create_dtm',
                            '$val->id_address_delivery',
                            '$val->amount',
                            '$val->delivery_fee',
                            '$val->payment_amount',
                            '$val->latitude',
                            '$val->longitude',
                            '$val->distance_delivery',
                            '$val->request_tax',
                            '$val->company_name',
                            '$val->company_address',
                            '$val->company_tax',
                            '$val->company_branch',
                            '$val->email',
                            '$val->address_gps'
                        )
                        ";

        $result =  DB::insert($sql);

        $order_id = DB::getPDO()->lastInsertId();
       
        if($order_id&&$val->cart_id){

            $dataCartDetail = DB::table('ev_carts_dtl')->where('cart_id',  $val->cart_id)->where('event_id',  $val->event_id)->get();

            
            foreach($dataCartDetail as $v){

                $sql = " INSERT INTO `ev_order_dtl`
                                    (
                                        `order_id`,
                                        `event_id`,
                                        `set_menu_id`,
                                        `order_no`,
                                        `order_date`,
                                        `price_per_unit`,
                                        `qty_unit`,
                                        `create_dtm`
                                    )
                                    VALUES
                                    (
                                        '$order_id',
                                        '$val->event_id',
                                        '$v->set_menu_id',
                                        '$val->order_no',
                                        '$val->order_date',
                                        '$v->price_per_unit',
                                        '$v->qty_unit',
                                        SYSDATE()
                                    )";

                $resultDtl =  DB::insert($sql);
                
                /**Insert Order Option Detial */

                $dataCartDetailOption = DB::table('ev_carts_options')->where('cart_dtl_id',  $v->cart_dtl_id)->where('cart_id',  $val->cart_id)->where('event_id',  $val->event_id)->get();

                if($dataCartDetailOption){

                    foreach($dataCartDetailOption as $opt){

                        $sql_option = " INSERT INTO `ev_order_options`
                                    (                                                            
                                    `order_id`,
                                    `event_id`,
                                    `set_menu_id`,
                                    `option_id`,
                                    `price_per_unit`,
                                    `qty_unit`,
                                    `create_dtm`
                                    )
                                    VALUES
                                    (                                     
                                        '$order_id',
                                        '$opt->event_id',
                                        '$opt->set_menu_id',
                                        '$opt->option_id',
                                        '$opt->price_per_unit',
                                        '$opt->qty_unit',
                                        SYSDATE()
                                    )";


                        $resultDtlOption =  DB::insert($sql_option);
                    }
                }
                

            }

            
        }

        return $order_id;

    }



    public function insertDeliveryAddress($val){

        $sql = " INSERT INTO `ev_delivery_address`
            (
            `member_id`,
            `firstname`,
            `lastname`,
            `email`,
            `tel`,
            `address`,
            `province_id`,
            `amphur_id`,
            `district_id`,            
            `zipcode`,
            `address_detail`,
            `status`,
            `create_dtm`
            )
            VALUES
            (         
            '$val->member_id',
            '$val->firstname',
            '$val->lastname',
            '$val->email',
            '$val->tel',
            '$val->address',
            '$val->province_id',
            '$val->amphur_id',
            '$val->district_id',
            '$val->zipcode',
            '$val->address_detail',
            1,
            SYSDATE()
            )";

        $result =  DB::insert($sql);
        $id_address = DB::getPDO()->lastInsertId();
        return $id_address;

    }

    public function checkInsertMember($val){


        $user = DB::table('ev_member_users')->where('email', $val->email)->first();

        if(!$user){


            $sql = " INSERT INTO `ev_member_users`
            (
            `username`,
            `name`,
            `email`,
            `password`,
            `created_at`
            )
            VALUES
            (         
            '$val->username',
            '$val->name',
            '$val->email',
            '$val->password',
            SYSDATE()
            )";

            $result =  DB::insert($sql);
            if($result){
                $id_member = DB::getPDO()->lastInsertId();
                return $id_member;
            }else{
                return false;
            }
            

        }

        return $user->id;


    }


    public function ev_request_tax($val){

        $sql = " INSERT INTO `ev_delivery_address`
            (
            `order_id`,
            `order_no`,
            `company_name`,
            `company_address`,
            `company_tax`,
            `company_branch`,
            `status`,
            `create_dtm`
            )
            VALUES
            (         
            '$val->order_id',
            '$val->order_no',
            '$val->company_name',
            '$val->company_address',
            '$val->company_tax',
            '$val->company_branch',
            1,
            SYSDATE()
            )";

        $result =  DB::insert($sql);

        return $result;

    }
    


    

}
