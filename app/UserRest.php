<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRest extends Model
{

    protected $table = 'rpm_rest_admin';
    protected $primaryKey = 'admin_id';

}
