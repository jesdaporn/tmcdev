var map;
var service;
var infowindow;
var marker;
var geocoder;

function initMap(div_map,div_box_search) {

    var bkk = new google.maps.LatLng(13.7645439, 100.565861);
    map = new google.maps.Map(
        document.getElementById(div_map), {
        //center: bkk, 
        zoom: 18,
        streetViewControl: false,
        mapTypeControl: false,
    }
    );

    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;

    // Create the search box and link it to the UI element.
    var input = document.getElementById(div_box_search);
    // var searchBox = new google.maps.places.SearchBox(input);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', function () {
        //infowindow.close();
        var place = autocomplete.getPlace();
        //console.log(place);
        if (!place.geometry) {
            return;
        }

        var pos = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };
        addMarker(pos)
    });


    getLocation();

}

function calculateAndDisplayRoute(pos_end) {

    var directionsRenderer = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;

    directionsRenderer.setMap(map);

    var pos_start = {
        lat: 13.7645439,
        lng: 100.565861
    };


    var start = pos_start;
    var end = pos_end;

    directionsService.route({
        origin: start,
        destination: end,
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            console.log(response);

            document.getElementById('place_route').innerHTML = "ระยะทาง : " + response.routes[0].legs[0].distance.text;

        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function geocodeLatLng(location) {

    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({ 'location': location }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                console.log(results[0]);

                var place_detail = document.getElementById('place_detail');
                place_detail.innerHTML = "<div><i class='fa fa-map-marker-alt'></i>" + results[0].geometry.location.lat() + " , " + results[0].geometry.location.lng() + "</div><div>" + results[0].formatted_address + "</div>";
                document.getElementById('inp_address').value = results[0].formatted_address;


                if (results[0].address_components) {
                    results[0].address_components.forEach(elm => {

                        elm.types.forEach(tpy => {
                            if (tpy == "postal_code") {
                                document.getElementById('inpzip_code_id').value = elm.long_name;
                            }

                        });
                    });

                }

                /*
                 Exp:data array address
                 0: {long_name: "20", short_name: "20", types: ["street_number"]}
                 1: {long_name: "ถนนรัชดาภิเษก", short_name: "ถนนรัชดาภิเษก", types: ["route"]}
                 2: {long_name: "แขวง ดินแดง", short_name: "แขวง ดินแดง",types: ["political", "sublocality", "sublocality_level_2"]}
                 3: {long_name: "เขตดินแดง", short_name: "เขตดินแดง", types: ["political", "sublocality", "sublocality_level_1"]}
                 4: {long_name: "กรุงเทพมหานคร", short_name: "กรุงเทพมหานคร", types:["administrative_area_level_1", "political"]}
                 5: {long_name: "ประเทศไทย", short_name: "TH", types: ["country", "political"]}
                 6: {long_name: "10400", short_name: "10400", types: ["postal_code"]}*/

            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });

    calculateAndDisplayRoute(location);
}


// Adds a marker to the map.
function addMarker(location) {

    if (marker) {
        marker.setMap();
    }


    marker = new google.maps.Marker({
        position: location,
        //label: labels[labelIndex++ % labels.length],
        map: map,
        draggable: true,
        //animation: google.maps.Animation.DROP,
    });
    map.setCenter(location);

    google.maps.event.addListener(marker, 'dragend', function () {
        map.setCenter(this.getPosition()); // Set map center to marker position            
        var pos = {
            lat: this.getPosition().lat(),
            lng: this.getPosition().lng()
        };
        //console.log(pos);
        geocodeLatLng(pos);
    });

    google.maps.event.addListener(map, "dragend", function () {
        marker.setPosition(this.getCenter());

        var pos = {
            lat: this.getCenter().lat(),
            lng: this.getCenter().lng()
        };
        //console.log(pos);
        geocodeLatLng(pos);

    });

    google.maps.event.addListener(marker, 'click', function () {
        /*
              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                'Place ID: ' + place.place_id + '<br>' +
            place.formatted_address + '</div>');
              infowindow.open(map, this);
              */
    });


    geocodeLatLng(location);

}



function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function getLocation() {

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            addMarker(pos);

        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}