import React from 'react';

class Province extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            province_id:"",
            province_items: [],
            amphur_items: [],
            district_items:[],
            zipcode_id:"" ,            
        };
        
    }

    componentDidMount() {
        const self = this;
        axios.get('/getProvince')
            .then(function (response) {

                console.log(response.data);
                self.setState({
                    province_items: response.data
                })

                let pro_id =document.getElementById('select_province_id').value;
                                             
                if(pro_id){
                    self.getAmphur(pro_id);
                }
                
                
               
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }


    handleChangeProvince(e) {
        e.preventDefault();
        this.getAmphur(e.target.value);
        
    }

    handleChangeAmphur(e) {
        e.preventDefault();
        this.getDistrict(e.target.value);
        
    }

    handleChangeDistrict(e){
        e.preventDefault();
        this.getZipCode(e.target.value);
    }

    getAmphur(province_id) {
        const self = this;
        axios.get('/getAmphur/' + province_id)
            .then(function (response) {
                // handle success
                self.setState({
                    amphur_items: response.data
                })
                let amp_id = document.getElementById('select_amphur_id').value;
                self.getDistrict(amp_id);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    getDistrict(amphur_id){
        const self = this;
        axios.get('/getDistrict/' + amphur_id)
            .then(function (response) {                
                self.setState({
                    district_items: response.data
                })

                let dist_id = document.getElementById('select_district_id').value;
                self.getZipCode(dist_id);
            })
            .catch(function (error) {                
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    getZipCode(amphur_id) {
        const self = this;
        axios.get('/getZipcode/' + amphur_id)
            .then(function (response) {
                
                self.setState({
                    zipcode_id: response.data.ZIPCODE
                })
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    render() {
        const { province_id, province_items, amphur_items, district_items,zipcode_id } = this.state;

        return <div className="row">
                    <div className="col-md-6 col-xs-12">
                        <div className="form-group">
                            <label className="control-label" htmlFor="select_province_id"><span title="จำเป็น">*</span>จังหวัด</label>
                            <select className="form-control" name="select_province_id" id="select_province_id" required="required" onChange={(e)=>this.handleChangeProvince(e)}>   
                                <option  value="">กรุณาเลือกจังหวัด</option>
                                {
                                province_items.map(item => (
                                    <option key={item.province_id} value={item.province_id}>{item.province_name}</option>
                                ))
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6 col-xs-12">
                        <div className="form-group">
                            <label className="control-label" htmlFor="select_amphur_id"><span title="จำเป็น">*</span>อำเภอ</label>
                            <select className="form-control" name="select_amphur_id" id="select_amphur_id" required="required" onChange={(e) => this.handleChangeAmphur(e)}>
                                <option value="">กรุณาเลือกอำเภอ</option>
                                {
                                    amphur_items ?
                                        amphur_items.map(item => (
                                            <option key={item.amphur_id} value={item.amphur_id}>{item.amphur_name}</option>
                                        ))
                                        : <option value="">ไม่พบ กรุณาเลือกอำเภอ</option>
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6 col-xs-12">
                        <div className="form-group">
                            <label className="control-label" htmlFor="select_district_id"><span title="จำเป็น">*</span>แขวง/ตำบล</label>
                            <select className="form-control" name="select_district_id" id="select_district_id" required="required" onChange={(e) => this.handleChangeDistrict(e)}>
                                <option value="">กรุณาเลือกแขวง/ตำบล</option>
                                {
                                    district_items ?
                                        district_items.map(item => (
                                            <option key={item.district_id} value={item.district_id}>{item.district_name}</option>
                                        ))
                                        : <option value="">ไม่พบ แขวง/ตำบล</option>
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6 col-xs-12">
                        <div className="form-group">
                            <label className="control-label" htmlFor="inpzip_code_id"><span title="จำเป็น">*</span>รหัสไปรษณีย์</label>
                    <input className="form-control" type="text" name="inpzip_code_id" id="inpzip_code_id"  value={zipcode_id} onChange={
                        (e) => { this.setState({ zipcode_id: e.target.value })}
                        } />
                        </div>
                    </div>

                
        </div>;
    }
}

export default Province;


