import React, { Component } from 'react'
import ReactDOM from 'react-dom';

class Mapplace extends Component {

    constructor(props) {
        super(props);
        
    }
          

    componentDidMount() {
        this.initMap()
    }

    componentWillUnmount() {
        
    }

    initMap() {
        var map;
        var service;
        var infowindow;
        var sydney = new google.maps.LatLng(-33.867, 151.195);

        infowindow = new google.maps.InfoWindow();

        map = new google.maps.Map(
            document.getElementById('map'), { center: sydney, zoom: 15 });

        var request = {
            query: 'Museum of Contemporary Art Australia',
            fields: ['name', 'geometry'],
        };

        service = new google.maps.places.PlacesService(map);

        service.findPlaceFromQuery(request, function (results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }

                map.setCenter(results[0].geometry.location);
            }
        });
    }

    createMarker(place) {
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }

    render() {

        return (
            <div>   
                <div style={{height:'100px',width:'100%'}} id="map">
                </div>
            </div>
        )


    }


}



export default Mapplace;

if (document.getElementById('map_place')) {
    ReactDOM.render(<Mapplace />, document.getElementById('map_place'));
}