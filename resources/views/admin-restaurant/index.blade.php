@extends('layouts.app_ryoii')

@section('content')
<div class="container mt-5" style="margin-auto;">
    <div class="card">
        <div class="card-header">
            <i class="fas fa-align-justify"></i> รายการสั่งซื้อทั้งหมด
        </div>
        <div class="card-body ">
            <?php 
                dump($Orders);    
            ?>

            <table class="table table-striped table-hover" style="border:1px solid #dee2e6;">
                <thead>
                    <tr class="text-white bg-secondary">
                        <th>ลำดับ</th>
                        <th>รายการ</th>
                        <th>วันที่สั่ง</th>
                        <th>วันที่จัดส่ง</th>
                        <th>จังหวัด</th>
                        <th>สถานะ</th>
                        <th>Tool</th>
                    </tr>
                </thead>
                <tbody>
                    
                        @foreach ($Orders as $item)
                        <tr>
                            <td style="width:45px;text-align:center;">1</td>
                            <td>
                                {{$item->order_no}}
                                <p class="card-text mb-0" style="font-size:11px;">
                                    <i class="far fa-comment-alt"></i>
                                    With supporting text below as a natural lead-in to additional content.
                                </p>
                            </td>
                            <td style="width:89px;text-align:center;">
                                {{$item->order_date}}
                            </td>
                            <td style="width:89px;text-align:center;">
                                {{$item->order_date}}
                            </td>
                            <td style="width:126px;text-align:center;">
                                กรุงเทพมหาคร
                            </td>
                            <td style="width:100px;">
                                <span class="badge badge-success">เข้ามาใหม่</span>
                            </td>
                            <td style="width:100px;">
                                <span class="badge badge-info"><i class="fas fa-clipboard-check"></i> รับออเดอร์</span>
                            </td>
                        </tr>
                        @endforeach

                    <tr>
                        <td style="width:45px;text-align:center;">1</td>
                        <td>
                            Penguin Eat Shabu - Next Day
                            <p class="card-text mb-0" style="font-size:11px;">
                                <i class="far fa-comment-alt"></i>
                                With supporting text below as a natural lead-in to additional content.
                            </p>
                        </td>
                        <td style="width:89px;text-align:center;">
                            26/06/2020
                        </td>
                        <td style="width:89px;text-align:center;">
                            28/06/2020
                        </td>
                        <td style="width:126px;text-align:center;">
                            กรุงเทพมหาคร
                        </td>
                        <td style="width:100px;">
                            <span class="badge badge-warning">กำลังดำเนินการ...</span>
                        </td>
                        <td style="width:100px;">
                            <span class="badge badge-primary"><i class="fas fa-shipping-fast"></i> จัดส่ง</span>
                        </td>
                    
                    </tr>

                    <tr>
                        <td style="width:45px;text-align:center;">1</td>
                        <td>
                            Penguin Eat Shabu - Next Day
                            <p class="card-text mb-0" style="font-size:11px;">
                                <i class="far fa-comment-alt"></i>
                                With supporting text below as a natural lead-in to additional content.
                            </p>
                        </td>
                        <td style="width:89px;text-align:center;">
                            26/06/2020
                        </td>
                        <td style="width:89px;text-align:center;">
                            28/06/2020
                        </td>
                        <td style="width:126px;text-align:center;">
                            กรุงเทพมหาคร
                        </td>
                        <td style="width:100px;">
                            <span class="badge badge-primary"> อยู่ระหว่างจัดส่ง</span>
                        </td>
                        <td style="width:100px;">

                        </td>

                    </tr>

                    <tr>
                        <td style="width:45px;text-align:center;">1</td>
                        <td>
                            Penguin Eat Shabu - Next Day
                            <p class="card-text mb-0" style="font-size:11px;">
                                <i class="far fa-comment-alt"></i>
                                With supporting text below as a natural lead-in to additional content.
                            </p>
                        </td>
                        <td style="width:89px;text-align:center;">
                            26/06/2020
                        </td>
                        <td style="width:89px;text-align:center;">
                            28/06/2020
                        </td>
                        <td style="width:126px;text-align:center;">
                            กรุงเทพมหาคร
                        </td>
                        <td style="width:100px;">
                            <span class="badge badge-success"> สำเร็จ</span>
                        </td>
                        <td style="width:100px;">
                            
                        </td>
                    
                    </tr>
                    
        
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection