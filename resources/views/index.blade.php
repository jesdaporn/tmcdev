@extends('layouts.app_ryoii')



@section('content')

<style>
    .card-title{font-weight: 600;}
    .container-bg-home{
        width:100%;
        height: 605px;
        background-image: url('/images/bg/bg_home.png');
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
       

    }
    .content-bg-home{
        padding-top:45px;        
    }
    .container-search-box{
        position: absolute;
        bottom:40px;
        width:100%;
        padding:25px 35px;
        background-color:#fff;
        border-radius:60px;
        -webkit-box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);
        -moz-box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);
        box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);
        display: flex;
    }
</style>


<div class="container-bg-home">
    <div class="container" style="height: 100%;position: relative;">
        <div class="content-bg-home">
            <div style="font-weight: 600;font-size:42px;max-width:500px;line-height:1.2;">Food delivery from the best restaurants near you.</div>
            <div style="font-size:56px;font-weight:bold;color:#e02610;">Order now!</div>
            <div style="font-size:19px;font-weight:bold;max-width:300px;">set exact location to find the right restaurants near tyou.</div>
        </div>     
        <div class="container-search-box">       
        
            <form style="width:100%;">
                <div style="width:100%;display:flex;">
                    <div class="col-8 d-flex" style="align-items: center;"> 
                        <i class="fas fa-map-marker-alt" style="color:#e02610;font-size:24px;margin-right:5px;"></i>
                        <input type="text" class="form-control" style="width:95%;border:none;" placeholder="Enter your name or delivery address">
                    </div>
                    <div class="col-4 d-flex">
                        <button type="submit" class="btn btn-primary" 
                        style="background-color: #e5223d;color: #fff;border: none;border-radius: 18px;padding: 10px 45px;font-size: 16px;">Delivery</button>
                        <span style="margin-left: 5px;margin-right:5px;font-size:20px;">|</span>
                        <button type="submit" class="btn btn-primary"
                            style="background-color: #e5223d;color: #fff;border: none;border-radius: 18px;padding: 10px 45px;font-size: 16px;">Pickup</button>
                    </div>
                </div>                    
            </form>

        </div>  
    </div>

</div>

<div style="background-color:#fff;width:100%;">
    <div class="container" style="display:flex;justify-content: space-between;align-items: center;height:125px;width:100%;">    
        <div>
            <img src="/images/btn/btn_1.png">
        </div>
        <div>
            <img src="/images/btn/btn_2.png">
        </div>
        <div>
            <img src="/images/btn/btn_3.png">
        </div>
        <div>
            <img src="/images/btn/btn_4.png">
        </div>

    </div>

</div>

<div class="mb-5" style="background-image:linear-gradient(to top, rgba(255,0,0,0), rgb(245, 242, 242));width:100%;padding-top:25px;">
    <div class="container">
        <h2 style="font-weight:bold;margin-bottom:1.5rem">
            Food Delivery
        </h2>

        <div class="row justify-content-left">
            @foreach($events as $event)
            <div class="col-md-3">
                <div class="card">
                    <a href="/post/{{$event->url_rewrite}}"><img src="{{config('app.url_ryoiireview').$event->event_poster}}"
                            class="card-img-top" alt=""></a>
                    <div class="card-body">
                        <a href="/post/{{$event->url_rewrite}}">
                            <h5 class="card-title" style="min-height: 42px;color:#212529;">{{$event->event_name_th}}</h5>
                        </a>
                        <p class="card-text" style="min-height: 49px;">{{Str::limit($event->description,55)}}</p>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="container justify-content-center mt-2">
            {{$events->links()}}
        </div>
    </div>

    
    

</div>





@endsection
