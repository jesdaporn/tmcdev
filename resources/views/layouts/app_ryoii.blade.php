<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ShopRyoiireview</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/v4-shims.css">


    <style>
        body{background: #fff;}
        #main {
            max-width: 1900px;
            margin: auto;
        }

        .max-content-top {
            width: 100%;
            max-width: 1300px !important;
            margin: auto;
        }

        footer.footer {
            padding-top: 2rem;                     
            background: #f8f9fa;
        }

        .navbar .navbar-collapse {

            border-top:1px solid #ededed;

        }
        .navbar-light{border-bottom: 1px solid #e5e5e6}
        .form-separator{
            font-size: 10px;
            line-height: 14px;
            color: #767676;
            text-align: center;
            display: inline-block;
            width: 100%;
            font-weight: 600;
            text-align: center;
        }

        .form-separator::before{display: block;content: "";border-bottom: 1px solid #e0e0e0;}

        .back-to-top{position: fixed;right:15px;bottom:15px;text-align: center;}

        .li-nav-menu{display: none;align-items:center;}
        .navbar-app{padding:1rem 0px;}

        .navbar-nav-app{flex-direction:row !important;justify-content: space-between;margin-top:1rem;}

        @media (min-width: 375px) {

        }

        @media (min-width: 414px) {

        }

        @media (min-width: 768px) {

            .navbar-nav-app{margin-top:0rem;}
            .li-nav-menu{display: flex;align-items:center;}

            .navbar .navbar-toggler {
                display: none;
            }

            .navbar .navbar-collapse {
                display: -ms-flexbox!important;
                display: flex!important;
                -ms-flex-preferred-size: auto;
                flex-basis: auto;
                border-color:transparent;
            }


            .navbar .navbar-nav {
                -ms-flex-direction: row;
                flex-direction: row;
            }
            .navbar .navbar-nav .nav-link {
                padding-right: .5rem;
                padding-left: .5rem;
            }



        }

        @media (min-width: 992px) {

        }

        @media (min-width:1024px) {


        }

        @media (min-width:1150px) {


        }
    </style>

    <!-- Scripts -->
   <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>
    <div id="app">

        <!-- Navigation -->
        <nav class="navbar navbar-light bg-light navbar-app">
            <div class="container">
                <a class="navbar-brand" href="/" style="font-size:24px;font-weight:600;"><img src="https://www.ryoiireview.com/images/ryoiireview-logo.png" style="width:89px;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto navbar-nav-app">
                        <li class="nav-item">
                            <a class="nav-link" href="/track-order"  style="font-weight: bold;color:#e3342f;font-size:14px;">
                                ติดตามคำสั่งซื้อ <i class="fas fa-truck"></i>
                            </a>
                        </li><!--
                        <li class="li-nav-menu"><a href="#" style="font-weight: bold;color:#495057;font-size:16px;">|</a></li>
                        <li class="nav-item">
                        {{--@if(AuthMember::get_member_userlogin_from_cookie())--}}
                                <div class="dropdown nav-link">
                                    <button class="btn btn-secondary dropdown-toggle" style="box-shadow:none;border: none;background:none;font-weight: bold;color:#495057;font-size:14px;padding:0px;" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-circle"></i> {{--Str::limit(AuthMember::get_member_userlogin_from_cookie()->email,10)--}}
                                    </button>
                                    
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="position: absolute;margn:0px;padding:0px;margin-top:-7px;">
                                        <div style="width: 0;height: 0; border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid rgba(0, 0, 0, 0.15);position: absolute;top:-5px;left:10px;">
                                        </div>
                                        <a class="dropdown-item" style="padding:0.25rem 0.5rem;color:#495057;" href="/profile"><i class="fas fa-address-card"></i> Profile</a>
                                        <div class="dropdown-divider m-0"></div>
                                        <a class="dropdown-item" style="padding:0.25rem 0.5rem;color:#495057;" href="/member-logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
                                    </div>
                                </div>
                            {{--@else--}}
                                <a class="nav-link" href="#" onclick="loginform()" style="font-weight: bold;color:#777777 ;font-size:14px;">
                                    Log in <i class="fas fa-user-circle"></i>
                                </a>
                                
                            {{--@endif--}}
                        </li>-->
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4" style="padding-top:0px !important;min-height:553px;">        
            @yield('content')
        </main>

        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 h-100 text-center my-auto">
                        
                        <div style="font-size:14px;font-weight:bold;margin-bottom:10px;">
                            AIA Capital Center Building,Room 1506,15th Floor 89 Ratchadapisek road,Din Daeng,Bangkok 10400<br>    
                            Tel: 02-042-7223-4
                        </div>
                        <p class="text-muted small mb-2 mt-2">&copy; Your Website 2020. All Rights Reserved.</p>
                    </div>
                    
                </div>
            </div>
        </footer>
        <a class="back-to-top" style="display: inline;" href="#"> <i class="fa fa-chevron-up"></i><BR /> TOP</a>
       

    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modal_login_ryoii">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" style="font-weight: 600;">RyoiiShop</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <!-- Start Form Login -->
                    <div class="form-login" id="form-app-login">
                        <div class="form-group">
                            <button onclick="checkStatusLogin()" class="btn btn-primary d-flex align-self-center justify-content-center " style="width:100%;">
                                <i class="fab fa-facebook fa-2x fa-fw" style="font-size: 26px;"></i>
                                <span style="margin-left:5px;font-size:17px;">ดำเนินการด้วย Facebook</span>
                            </button>
                        </div>
                        <div class="form-separator">
                            <span>หรือ</span>
                        </div>

                        <form>
                            <div class="form-group">
                                <label for="inp_login_email" style="font-weight: bold;margin-lelt:5px;">Email</label>
                                <input type="email" class="form-control" id="inp_login_email">
                                <div class="d-none" id="alert-login-email">
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong id="alert-login-email-text">*กรุณาใส่ Email !!!</strong>
                                    </span>                              
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="inp_login_password" style="font-weight: bold;margin-lelt:5px;">Password</label>
                                <input type="password" class="form-control" id="inp_login_password">
                                <div class="d-none" id="alert-login-password">
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong id="alert-login-password-text">*กรุณาใส่ Password !!!</strong>
                                    </span>
                                </div>                                                             
                            </div>                            

                            <button type="button" class="btn btn-primary" style="width:100%;margin-bottom:10px;" onclick="memberLogin()">เข้าสู่ระบบ</button>
                        </form>

                        <p class="form-group" style="font-size: 11px;margin-bottom:5px;">
                            <a style="margin-left:5px;color:#2f2828;font-weight: 600;" href="#" onclick="resetForm()">ลืมรหัสผ่านใช่ไหม</a>
                        </p>
                        <p class="form-group" style="font-size: 11px;margin-bottom:5px;">
                            <span style="margin-left:5px;">ไม่มีบัญชีใช่ไหม <a href="#" onclick="registerForm()">สมัคร</a></span>
                        </p>
                    </div>
                    <!-- End Form Login -->

                    <!-- Start Form Register -->
                    <div class="form-reginster d-none" id="form-app-register">
                        <div class="form-group">
                            <label style="font-weight: bold;">สมัครสมาชิก - ฟรี!</label>
                        </div>
                        <div class="form-group">
                            <label for="inp_register_email" style="font-weight: bold;margin-lelt:5px;">Email</label>
                            <input type="email" class="form-control" id="inp_register_email">
                            <div class="d-none" id="alert-register-email">
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong id="alert-register-email-text">*กรุณาใส่ Email !!!</strong>
                                </span>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label for="inp_register_password" style="font-weight: bold;margin-lelt:5px;">Password</label>
                            <input type="password" class="form-control" id="inp_register_password">
                            <div class="d-none" id="alert-register-password">
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong id="alert-register-password-text">*กรุณาใส่ Password !!!</strong>
                                </span>
                            </div>
                        </div>

                        

                        <button type="button" class="btn btn-success" onclick="registerMember()" style="width:100%;margin-bottom:10px;">สมัครสมาชิก</button>
                    
                        <p class="form-group" style="font-size: 11px;margin-bottom:5px;">
                            <a style="margin-left:5px;color:#2f2828;font-weight: 600;" href="#" onclick="loginForm()">ย้อนกลับ</a>
                        </p>
                    </div>
                    <!-- End Form Register -->

                    <!-- Start Reset Password -->
                    <div class="form-reset-password d-none" id="form-app-reset">
                        
                        <form>
                            <div class="form-group">
                                <label style="font-weight: bold;">ลืมรหัสผ่านของคุณ?</label>
                                <p>
                                    ลิงก์จะถูกส่งไปยังอีเมล์ที่ระบุเพื่อตั้งรหัสผ่านใหม่
                                </p>
                            </div>
                            <div class="form-group">
                                <label for="inp_reset_email" style="font-weight: bold;margin-lelt:5px;">Email</label>
                                <input type="email" class="form-control" id="inp_reset_email" aria-describedby="emailHelp">
                                <div class="d-none" id="alert-reset-email">
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong id="alert-reset-email-text">*กรุณาใส่ Email !!!</strong>
                                    </span>
                                </div>
                            </div>

                            <button type="button" class="btn btn-info" style="width:100%;margin-bottom:10px;" onclick="resetEmailMember()">ส่งลิงก์</button>
                        </form>
                        <p class="form-group" style="font-size: 11px;margin-bottom:5px;">
                            <a style="margin-left:5px;color:#2f2828;font-weight: 600;" href="#" onclick="loginForm()">ย้อนกลับ</a>
                        </p>
                    </div>
                    <!-- End Reset Password -->


                </div>

            </div>
        </div>
    </div>

<script>
    /* $(function() {
                $('.back-to-top').hide();
                $(document).on('scroll', function() {
                    if ($(window).scrollTop() > 400 && $(window).width() > 750) {
                        $('.back-to-top').show();
                    } else {
                        $('.back-to-top').hide();
                    }
                });
                $('.back-to-top').on('click', scrollToTop);
            });

            function scrollToTop() {
                $('html, body').animate({
                    scrollTop: 0
                }, 300, 'linear');
            }*/

            const loginform=()=>{
                $("#modal_login_ryoii").modal('show');
            }

            const registerForm=()=>{
                document.getElementById('form-app-login').style.cssText = 'display:none !important';
                document.getElementById('form-app-register').style.cssText = 'display:block !important';
                document.getElementById('form-app-reset').style.cssText = 'display:none !important';

                displayAllNone();
            }

            const loginForm=()=>{
                document.getElementById('form-app-login').style.cssText = 'display:block !important';
                document.getElementById('form-app-register').style.cssText = 'display:none !important';
                document.getElementById('form-app-reset').style.cssText = 'display:none !important';

                displayAllNone();
            }

            const resetForm=()=>{
                document.getElementById('form-app-login').style.cssText = 'display:none !important';
                document.getElementById('form-app-register').style.cssText = 'display:none !important';
                document.getElementById('form-app-reset').style.cssText = 'display:block !important';

                displayAllNone();
            }

            const displayAllNone = ()=>{
                document.getElementById('alert-register-email').style.cssText = 'display:none !important';
                document.getElementById('alert-register-password').style.cssText = 'display:none !important';
                document.getElementById('alert-login-email').style.cssText = 'display:none !important';
                document.getElementById('alert-login-password').style.cssText = 'display:none !important';
            }



            const memberLogin=()=>{

                let login_email = document.getElementById('inp_login_email').value;
                let login_password = document.getElementById('inp_login_password').value;

                document.getElementById('alert-login-email').style.cssText = 'display:none !important';
                document.getElementById('alert-login-password').style.cssText = 'display:none !important';

                if(!login_email){
                    document.getElementById('alert-login-email').style.cssText = 'display:block !important';
                    document.getElementById('alert-login-email-text').innerHTML="กรุณาใส่ Email";
                }
                if(!login_password){                    
                    document.getElementById('alert-login-password').style.cssText = 'display:block !important';  
                    document.getElementById('alert-login-password-text').innerHTML="กรุณาใส่ Password";      
                }


                if(login_email&&login_password){

                    axios.post('/member-login', {
                        email: login_email,
                        password: login_password
                    })
                    .then(function (response) {

                        if(response.data){
                           let data = response.data;
                           console.log(data);                          

                           if(data.status!="success"){
                            
                                if(data.msg.email){
                                    document.getElementById('alert-login-email').style.cssText = 'display:block !important';
                                    data.msg.email.forEach((val,index)=>{
                                        document.getElementById('alert-login-email-text').innerHTML=val;
                                    });
                                }

                                if(data.msg.password){
                                    document.getElementById('alert-login-password').style.cssText = 'display:block !important';
                                    data.msg.password.forEach((val,index)=>{
                                       document.getElementById('alert-login-password-text').innerHTML=val;
                                    });                                   
                                }                            
                            
                            }else{
                                /** case Login success */
                                location.reload();                        
                            }

                            
                        }
                        
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                }
                
            }

            const registerMember=()=>{

                let register_email = document.getElementById('inp_register_email').value;
                let register_password = document.getElementById('inp_register_password').value;
                
                document.getElementById('alert-register-email').style.cssText = 'display:none !important';
                document.getElementById('alert-register-password').style.cssText = 'display:none !important';
                
                if(!register_email){
                    document.getElementById('alert-register-email').style.cssText = 'display:block !important';
                    document.getElementById('alert-register-email-text').innerHTML="กรุณาใส่ Email";
                }
                if(!register_password){
                    document.getElementById('alert-register-password').style.cssText = 'display:block !important';
                    document.getElementById('alert-register-password-text').innerHTML="กรุณาใส่ Password";
                }
            
                if(register_email&&register_password){
                    axios.post('/member-register', {
                        email: register_email,
                        password: register_password
                    })
                    .then(function (response) {
                        
                        if(response.data){
                        
                            let data = response.data;
                            console.log(data);

                            if(data.status!="success"){
                        
                                if(data.msg.email){
                                    document.getElementById('alert-register-email').style.cssText = 'display:block !important';
                                    data.msg.email.forEach((val,index)=>{
                                        document.getElementById('alert-register-email-text').innerHTML=val;
                                    });
                                }
                            
                                if(data.msg.password){
                                    document.getElementById('alert-register-password').style.cssText = 'display:block !important';
                                    data.msg.password.forEach((val,index)=>{
                                        document.getElementById('alert-register-password-text').innerHTML=val;
                                    });
                                }
                                
                            }else{
                            /** case Login success */
                                alert(data.msg);
                                location.reload();
                            }
                        
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
                                                        
            }


            const resetEmailMember=()=>{


                document.getElementById('alert-reset-email').style.cssText = 'display:none !important';

                let email_reset = document.getElementById('inp_reset_email').value;

                if(email_reset){
                        axios.post('/member-reset-password', {
                        email: email_reset
                     })
                    .then(function (response) {
                        
                        if(response.data){

                            let data = response.data;
                            console.log(data);
                           
                            if(data.status!="success"){
                            
                                if(data.msg.email){
                                    document.getElementById('alert-reset-email').style.cssText = 'display:block !important';
                                    data.msg.email.forEach((val,index)=>{
                                        document.getElementById('alert-reset-email-text').innerHTML=val;
                                    });
                                }else{
                                    alert(data.msg);
                                    location.reload();
                                }
                                                        
                            }else{
                               
                                alert(data.msg);
                                location.reload();
                            }

                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }else{
                   document.getElementById('alert-reset-email').style.cssText = 'display:block !important';
                   document.getElementById('alert-reset-email-text').innerHTML = 'กรุณาใส่ Email';
                }
                
            }

            const checkStatusLogin = ()=>{

                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        apifb();
                    }else{
                        fblogin();
                    }
                });
            }

            const fblogin=()=>{
                FB.login(function(response) {
                if (response.authResponse) {
                    apifb();              
                }
                },{
                    scope: 'public_profile,email,user_gender'
                });
            }

            const apifb=()=>{
                FB.api('/me?fields=id,email,name,first_name,last_name,picture,short_name,birthday,gender', function(response) {
                            
                    console.log(response);
                    var profile = {};
                    profile.id=response.id;
                    profile.email=response.email;
                    profile.name=response.name;
                    profile.first_name=response.first_name;
                    profile.last_name=response.last_name;
                    profile.birthday=response.birthday;
                    profile.gender=response.gender;
                    profile.picture=response.picture.data.url;
                    profile.login_by="facebook";

                    if(profile){
                        axios.post('/social-login', {
                            profile: profile,
                        })
                        .then(function (response) {
                        
                            if(response.data){
                            
                                let data = response.data;
                                console.log(data);       
                                if(data.status=="success"){
                                   
                                    location.reload();
                                } else{
                                    alert(data.msg);
                                    location.reload();
                                }                    
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    }
           
                });
            }

            const fblogout=()=>{
                FB.getLoginStatus(function(response) {
                    //console.log(response);
                    if (response.status === 'connected') {
                        FB.logout(function(response) {
                        //console.log(response);
                    });
                    }
                });
            }
    </script>
   
    <script>
        window.fbAsyncInit = function() {
        FB.init({
          appId      : '811405536063239',
          xfbml      : true,
          version    : 'v7.0'
        });
        //FB.AppEvents.logPageView();
      };
    
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

</body>

</html>
