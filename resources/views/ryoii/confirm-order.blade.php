@extends('layouts.app_ryoii')

@section('content')
<style>

    label{font-weight: bold;}

    .body-card-timer{padding:15px 35px;display: block;align-items:center;}
    .card-timer{font-size:32px;font-weight:bold;color:#3490dc;text-align:center;padding:15px;}
    .card-detail-timer{font-size:16px;color:#3490dc;padding:0px;padding-bottom:15px;}
    
    #map_place{height: 400px;width: 100%;}

    @media (min-width: 1200px){
       
    }
    @media (min-width: 768px) {
        .body-card-timer{display: flex;align-items:center;}
        .card-timer{padding:0px;padding-right:35px;}
        .card-detail-timer{padding:0px;}
    }

    @media (min-width: 574px) {

    }



</style>

<div style="background-color: #fff;width:100%;height:100%;font-family: Tahoma, Geneva, Verdana, sans-serif;padding-top:35px;">
    
    <div style="max-width:926px;margin:auto;">

        <!-- Navbar Status -->
        <div class="container mb-5">
            <div class="d-flex justify-content-center">
                <div class="d-flex align-items-center">
                    <span class="badge badge-pill badge-success">1</span>
                    <div style="border-bottom:1px dotted ;width:100px;height:2px;margin-left:5px;margin-right:5px; ">
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <span class="badge badge-pill badge-secondary">2</span>
                    <div style="border-bottom:1px dotted ;width:100px;height:2px;margin-left:5px;margin-right:5px; "></div>
                </div>
                <div class="d-flex align-items-center">
                    <span class="badge badge-pill badge-secondary">3</span>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div style="width:130px;text-align:center;">
                    <a href="#" style="color:#000106; ">รายการสั่งซื้อ</a>
                </div>
                <div style="width:130px;text-align:center;">
                    <a href="#" style="color:#6c757d;pointer-events: none;cursor: default; ">ชำระเงิน</a>
                </div>
                <div style="width:130px;text-align:center;">
                    <a href="#" style="color:#6c757d;pointer-events: none;cursor: default; ">รายการสำเร็จ</a>
                </div>
            </div>
        
        </div><!-- End Navbar Status -->
        
        <?php
            //dump($cartItems);
        ?>
        
        <!-- Display Coundown Timer -->
        <div class="container mb-5">
            <div class="card">
                <div class="card-body" style="padding:0px;">
                    <div class="body-card-timer">
                        <div class="card-timer" id="timer-countdown">
                            15:00
                        </div>
                        <div style="font-size:16px;color:#3490dc;">กรุณาทำรายการภายใน 15 นาที
                            หากท่านไม่ได้ทำรายการให้เสร็จภายในเวลาที่กำหนด
                            สิทธิ์การสั่งซื้อทั้งหมดจะถูกปล่อยให้ผู้ใช้งานท่านอื่น</div>
                    </div>                
                </div>
            </div>
        </div><!-- End Display Coundown Timer -->

        <!-- Panel Brand Product -->
        <div class="container mb-5 d-flex align-items-center">
            <div style="width:250px;">
                <img src="{{config('app.url_ryoiireview').$cartItems->event_poster}}" class="img-fluid">
            </div>
            <div class="container" style="width:100%;">
                <div style="font-size:19px;font-weight:bold;font-family:tahoma, Geneva, Verdana, sans-serif">
                    {{$cartItems->event_name_th}}
                </div>
                <div style="font-size:14px;font-family:tahoma, Geneva, Verdana, sans-serif;">
        
                </div>
        
            </div>
        </div>
        <!-- End Panel Brand Product -->
        
        <!-- Address Old Database -->
        <div class="container mb-5">
        
            <div class="user-address panel d-none" data-address-id="14280">
                <div class="panel-body">
                    <div class="row">
                        <label class="col-xs-12 font-weight-normal">
                            <div class="flex-container">
                                <div class="flex-1">
                                    <div class="d-inline-block align-top mr-2">
                                        <input type="radio" value="14280" name="order[use_address_id]"
                                            id="order_use_address_id_14280" style="">
                                    </div>
                                    <div class="d-inline-block user-address-info">
                                        เจษฎาพร มั่งมี<br>
                                        89 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400 อาคาร AIA Capitalcenter ถนน
                                        ดินแดง <br>
                                        ดินแดง / Dindang ดินแดง / Din Daeng กรุงเทพมหานคร / Bangkok<br>
                                        10400<br>
        
                                    </div>
                                </div>
        
                                <a class="edit-user-address" data-remote="true" data-method="get"
                                    href="/addresses/14280/edit?in_order=true">แก้ไข</a>
                            </div>
                        </label> </div>
                </div>
            </div>
        
        
        </div>
        <!-- Address Old Database -->
        
        <!--
        <div class="container mb-5" style="margin-top:5rem;margin-bottom:5rem;">
            <div class="form-group">
                <label class="control-label">
                    <span title="required">*</span> ช่องทางการจัดส่ง
                </label>
                <div class="form-group">
                    <input type="hidden"  value="161">
                    <div class="mb-4">
                        <div class="pr-1" style="align-self: center;">
                            <input type="radio" name="shipping_couriers" id="shipping_couriers_penguin" value="penguin">
                        </div>
                        <label for="shipping_couriers_penguin" class="flex-1 mb-0 ml-2">
                            <div class="media" data-courier-key="penguin" data-courier-id="161">
                                <div class="media-left">
                                    <img width="80"
                                        src="https://p-u.popcdn.net/couriers/logos/000/000/003/thumb/82a966dbbb7bf342c2af3cc2ba3a9a622e84c9e3.jpg?1586784425"
                                        alt="82a966dbbb7bf342c2af3cc2ba3a9a622e84c9e3">
                                </div>
                                <div class="media-body media-middle">
                                    <h4 class="media-heading">Penguin</h4>
                                    <p class="mb-0">
                                        ค่าบริการจัดส่ง : <strong>฿70.00</strong>
                                    </p>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>-->
        
        
        <div class="container mb-2">
            <h2 style="margin-bottom:0.5rem;">ที่อยู่การจัดส่ง</h2>
            <h3 style="color:#e3342f;margin-bottom:1.5rem;font-size:16px;font-weight:bold;">สินค้าจัดส่งในวัน เสาร์ที่
                25/07/2020</h3>
            <div class="form-group">
                <input class="form-control" placeholder="ป้อนตำแหน่งเพื่อระบุที่หมายการจัดส่ง" id="inp_search_place">
            </div>
            <div id="map_place">
        
            </div>
            <div class="mt-2 mb-2">
                <div class="d-flex justify-content-between" style="width:100%;">
                    <button class="btn btn-info" onclick="getLocation()">ปักที่อยู่ปัจจุบัน</button>
                    <!--<button class="btn btn-success" style="width:70%;">เลือกที่อยู่นี้</button>-->
                </div>        
            </div>
            <div>
                @if ($errors->any())
                <script>
                    alert('{{$errors->all()[0]}}');
                </script>
                @endif
            </div>
            <div class="card mb-2">
                <div class="card-body">
                    <div id="place_detail" style="font-weight: 600;color:#212529;">
        
                    </div>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-body" id="place_route" style="font-weight: 600;color:#212529;">
                    <div id="route_delivery">
                        ระยะทาง
                    </div>
                    <div id="price_delivery">

                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="/order-payment" onsubmit="return onSubmitOrder()" autocomplete="off">
            @csrf
            <input type="hidden" name="latitude_delivery" id="latitude_delivery">
            <input type="hidden" name="longitude_delivery" id="longitude_delivery">
            <input type="hidden" name="route_delivery_map" id="route_delivery_map">
            <input type="hidden" name="cart_id" id="cart_id" value="{{$cartItems->cart_id}}">            
            <input type="hidden" name="event_id" id="event_id" value="{{$cartItems->event_id}}">
            <input type="hidden" name="address_gps" id="address_gps" />

        <div class="container mb-5">
            <div class="card mb-2">
                <div class="card-body">
                    <div class="row ">                            
                       
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inp_firstname"><span title="จำเป็น">*</span>ชื่อ</label>
                                <input class="form-control" autocomplete="off"  type="text" name="inp_firstname" id="inp_firstname" required="required">
                            </div>
                        </div>
        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inp_lastname"><span title="จำเป็น">*</span> นามสกุล</label>
                                <input class="form-control" autocomplete="off" type="text" name="inp_lastname" id="inp_lastname" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inp_email"><span title="จำเป็น">*</span> Email</label>
                                <input class="form-control" autocomplete="off" type="email" name="inp_email" id="inp_email"  placeholder="email@email.com"
                                    required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inp_tel"><span title="จำเป็น">*</span> เบอร์โทรศัพท์มือถือ</label>
                                <input class="form-control" autocomplete="off" type="tel" name="inp_tel" id="inp_tel"  placeholder="089 999 9999" required="required">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inp_address"><span title="จำเป็น">*</span> เลขที่, อาคาร/หมู่บ้าน, ถนน</label>
                                <input class="form-control" autocomplete="off" type="text" name="inp_address" id="inp_address"  placeholder="" required="required">
                            </div>
                        </div>
                                
        
                       <!-- Call Fanction On React JS (resources/js/components/address) -->
                        <div class="col-md-12 col-xs-12" id="div_province_select"></div>
                        <!-- End Call Fanction On React JS Componend (resources/js/components/address) -->
                       

                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="inp_detail_address">รายละเอียดที่อยู่เพิ่มเติม</label>
                                <textarea class="form-control" id="inp_detail_address" name="inp_detail_address" rows="3"
                                    placeholder="เช่น ชื่อตึก ชื่อคอนโด เลขที่ห้อง จุดสังเกตใกล้เคียง เป็นต้น"></textarea>
                            </div>
                        </div>
        
                        <!--<div class="col-md-12">
                            <label class="boolean" for="order_save_address">
                                <input type="checkbox" value="1" name="order_save_address" id="order_save_address" > บันทึกที่อยู่นี้
                            </label>
                        </div>-->

                    </div>
                </div>
            </div>
        </div>
        
        <div class="container" style="margin-top:5rem;margin-bottom:5rem;">
            <h2>
                ขอใบกำกับภาษี
            </h2>
            <div class="form-group">
                <div class="checkbox" >
                    <input type="checkbox" value="1" name="order_request_tax" id="order_request_tax" data-toggle="collapse" data-target="#collapseOrderTax" aria-expanded="false" aria-controls="collapseOrderTax">
                    <label for="order_request_tax">ต้องการขอใบกำกับภาษีและใบเสร็จ</label>
                </div>
                <div class="collapse" id="collapseOrderTax">
                    <div class="card card-body">
                        <div class="form-group">
                            <label for="companyName">ชื่อบริษัท*</label>
                            <input type="text" class="form-control" name="companyName" id="companyName" placeholder="ชื่อบริษัท">                            
                        </div>
                        <div class="form-group">
                            <label for="CompanyAddress">ที่อยู่บริษัท*</label>
                            <input type="text" class="form-control" id="CompanyAddress" name="CompanyAddress" placeholder="ที่อยู่บริษัท">                            
                        </div>      
                        <div class="form-group">
                            <label for="CompanyTax">เลขประจำตัวผู้เสียภาษี*</label>
                            <input type="text" class="form-control" id="CompanyTax" name="CompanyTax" placeholder="เลขประจำตัวผู้เสียภาษี">
                        </div>
                        <div class="form-group">
                            <label for="idBranch">ระบุรหัสสาขา (ถ้ามี)</label>
                            <input type="text" class="form-control" id="idBranch" name="idBranch" placeholder="ระบุรหัสสาขา (ถ้ามี)">
                        </div>                  
                    </div>
                </div>
            </div>
        
        </div>
        
        
        <div class="container mb-5">
        
            <h2 style="margin-bottom:15px;">ตรวจสอบสรุปรายการคำสั่งซื้อ</h2>
            <h3 style="color:#e3342f;margin-bottom:1.5rem;font-size:16px;font-weight:bold;">สินค้าจัดส่งในวัน เสาร์ที่
            25/07/2020</h3>
            <h3 class="title-order-id" style="margin-bottom:1.5rem;">
                คำสั่งซื้อ 
            </h3>
        
            <div>
                <div class="table-responsive">
                    <table class="table" style="border-bottom: 2px solid #212529;">
                        <thead>
                            <tr>
                                <th style="width:35%;text-align:center;">รายการ</th>
                                <th style="text-align: center;">ราคา</th>
                                <th style="text-align: center;">จำนวน</th>
                                <th style="text-align: center;">รวม</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totalPrice=0;?>
                            @foreach($cartItems->cart_detail as $item)                                                                                      
                                <tr>
                                    <td style="padding:0.25rem;"><small><strong>{{$item->menu_name_th}}</strong></small></td>
                                    <td style="text-align: right;padding:0.25rem;">
                                        <small>
                                            ฿{{number_format($item->price_per_unit,2)}}
                                        </small>
                                    </td>
                             
                                    <td style="text-align: right;padding:0.25rem;">
                                        <small>{{$item->qty_unit}}</small>
                                    </td>
                                    <td style="text-align: right;padding:0.25rem;">
                                        <small>
                                            ฿{{number_format($item->price_per_unit*$item->qty_unit,2)}}
                                        </small>
                                    </td>
                                    
                                </tr>
                                
                                @if($item->cart_option)                             
                                    @foreach($item->cart_option as $opt)                                
                                    <tr>
                                        <td style="padding:0.25rem;font-size:12px;border-top:none;padding-left:25px;">
                                            +{{$opt->option_name}}
                                        </td>
                                        <td style="padding:0.25rem;font-size:12px;border-top:none;text-align:right;">
                                            ฿{{number_format($opt->option_price,2)}}
                                        </td>
                                        <td style="padding:0.25rem;font-size:12px;border-top:none;text-align:right;">
                                            {{$opt->qty_unit}}
                                        </td>
                                        <td style="padding:0.25rem;font-size:12px;border-top:none;text-align:right;">
                                            ฿{{number_format($opt->option_price*$opt->qty_unit,2)}}
                                        </td>
                                    </tr>
                                    @endforeach
                        
                                
                                @endif
                                
                               <?php $totalPrice+=$item->price_per_unit*$item->qty_unit; ?>
                            @endforeach
                            
                            <tr >
                                <td colspan="4"></td>
                            </tr>
        
                            <tr >
                                <td colspan="3" >
                                    <strong>รวม</strong>
                                </td>
                                <td style="text-align: right;">
                                    <small>฿{{number_format($cartItems->amount,2)}}</small>
                                </td>
                                
                            </tr>
        
                            <tr >
                                <td colspan="3">
                                    <strong>ค่าขนส่ง (3 กิโลเมตรแรกจากร้าน ฟรี กม.ต่อไป กม.ละ 10 บาท)</strong>
                                </td>
                                <td style="text-align: right;">
                                    <small>฿<span id="distance_order">00.00</span></small>
                                </td>
                                
                            </tr>
        
        
                            <tr class="add-top-border">
                                <td  colspan="3">
                                    <strong class="text-lg">รวมทั้งหมด</strong>
                                </td>
                                <td style="text-align: right;">
                                    <strong class="text-lg" >฿<span id="total_price_order">{{number_format($cartItems->amount,2)}}</span></strong>
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
        

            </div>
        
            <div class="text-danger mb-4">
                <small>
                 <div style="font-weight: bold;color:#000106;"><a href="/policy" target="_blank">เงื่อนไขการใช้บริการและโปรโมชั่น</a></div>               
                </small>
            </div>

            <input type="hidden" name="totalPayment" id="totalPayment" value="{{$totalPrice}}">
            <input type="hidden" name="delivery_fee" id="delivery_fee" value="0">
                <div class="mt-2 mb-5">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="accept_terms" id="accept_terms" value="1" required="required" style="margin-right:5px;">ฉันยอมรับ ข้อตกลงและเงื่อนไข
                    </label>
                </div>
                
                <div class="row">
                    <div class="col-6">
                        <button onclick="cancelOrder()" type="button" class="btn btn-secondary btn-lg "                       
                        >ยกเลิกการสั่งซื้อ</button>
                    </div>
            
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-lg  btn-success" id="btn_confirm_order">
                            <span>ยืนยันการสั่งซื้อ</span>
                        </button>
            
                        <div class="visible-ts mb-3"></div>
                    </div>
                </div>
        
            </div>
        </form>
     </div>   

</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBk6yaRjPzFwRbtU8VPdAB-hh_SLxcLndg&libraries=places" async defer></script>
<script>

    var map;
    var service;
    var infowindow;
    var marker;
    var geocoder;
    setTimeout(() => {

    }, 500);
        window.onload = function () {
            var display = document.querySelector('#timer-countdown');

            initMap();
          
            @if($cartItems->checkPeriod)
                //========== between start time to end time
                var period_time ={{$cartItems->end_time*1000}}-new Date().getTime()

                //time(seconds)
                console.log({{$cartItems->end_time}});
                console.log(period_time);
                console.log(new Date().getTime());
                var NumberMinutes = period_time/1000;

                console.log({{$cartItems->end_time}});
                console.log(NumberMinutes);
                console.log("-----*-----");
                startTimer(NumberMinutes, display);
            @else
                exitSession();
            @endif

        };

    function onSubmitOrder(){


        var checktax=document.getElementById('order_request_tax').checked;
        var inp_address_ges = document.getElementById('address_gps').value;


        if(!inp_address_ges){
            alert("กรุณาป้อนตำแหน่งเพื่อระบุที่หมายการจัดส่ง");
            return false;
        }

        if(checktax){
            var company_name = document.getElementById('companyName').value;
            var company_address = document.getElementById('CompanyAddress').value;
            var company_tax = document.getElementById('CompanyTax').value;
            var company_branch = document.getElementById('idBranch').value;
           
            if(!company_name||!company_address||!company_tax){
                document.getElementById('companyName').focus();
                alert("กรุณากรอกข้อมูลขอใบกำกับภาษีให้ครบ");
                return false;
            }

            var checkedterms = document.getElementById('accept_terms').checked;

            if(!checkedterms){
                alert("กรุณากด ยอมรับ ข้อตกลงและเงื่อนไขก่อนยืนยันการสั่งซื้อ");
                return false;
            }
                        
            
        }
        
        return true;


        

    }

    function cancelOrder(){
        if(confirm("คุณต้องการยกเลิกรายการสั่งซื้อ?")){
            location.href="/cancel-order";
        }
        
    }


    function initMap() {

        var AIA = new google.maps.LatLng(13.7646099, 100.5682175);
        map = new google.maps.Map(
            document.getElementById('map_place'), {
                center: AIA, 
                zoom: 18,
                streetViewControl: false,
                mapTypeControl: false,
            }
        );
        
        var infowindow = new google.maps.InfoWindow;
       
        // Create the search box and link it to the UI element.
        var input = document.getElementById('inp_search_place');
        // var searchBox = new google.maps.places.SearchBox(input);
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        autocomplete.addListener('place_changed', function() {
            //infowindow.close();
            var place = autocomplete.getPlace();
            //console.log(place);
            if (!place.geometry) {
                return;
            }

            var pos = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            };
            addMarker(pos)
        });


        //getLocation();
        
    } 

 

    function addCommas(nStr)
    {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }


    function calculateDelivery(val){

        //=============== 3 กิโลแรก ฟรี กินโลต่อไปกิโล ละ 10 =============

        var distance = val;
        var price_bath = 50;
        if(distance<=3000){
            price_bath = 0;
        }else{
            distance = distance-3000;
            var num_cal = distance/1000;
            var num_ceil = Math.ceil(num_cal);
            price_bath = num_ceil*10;
        }

        return price_bath.toFixed(0);
    }

    function geocodeLatLng(location) {
       
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'location': location}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    console.log(results[0]);
   
                    var place_detail = document.getElementById('place_detail');
                    place_detail.innerHTML="<div><i class='fa fa-map-marker-alt'></i>"+results[0].geometry.location.lat()+" , "+results[0].geometry.location.lng()+"</div><div>"+results[0].formatted_address+"</div>";
                    document.getElementById('inp_address').value=results[0].formatted_address;
                    document.getElementById('address_gps').value=results[0].formatted_address;
                    
                   
                    if(results[0].address_components){
                        results[0].address_components.forEach(elm => {
                            
                            elm.types.forEach(tpy => {
                                if(tpy=="postal_code"){
                                    document.getElementById('inpzip_code_id').value=elm.long_name;
                                }
                                    
                            });
                        });
                        
                    }

                    document.getElementById('longitude_delivery').value=results[0].geometry.location.lng();
                    document.getElementById('latitude_delivery').value=results[0].geometry.location.lat();                    
                            
                    
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });

        calculateAndDisplayRoute(location);
    }

function calculateAndDisplayRoute(pos_end){

    var directionsRenderer = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;

    directionsRenderer.setMap(map);

    var pos_start = {
        lat: 13.7895712,
        lng: 100.5484011
    };


    var start = pos_start;
    var end =pos_end;

    directionsService.route({
        origin: start,
        destination: end,
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            console.log(response);

            document.getElementById('route_delivery').innerHTML="ระยะทาง : "+response.routes[0].legs[0].distance.text;
            var price_distance = calculateDelivery(response.routes[0].legs[0].distance.value);

            document.getElementById('price_delivery').innerHTML="ค่าจัดส่ง : "+price_distance+" บาท";
            document.getElementById('distance_order').innerHTML=price_distance;
            var price_order = '<?php echo $cartItems->amount; ?>';
            document.getElementById('total_price_order').innerHTML=addCommas((parseFloat(price_order)+parseFloat(price_distance)).toFixed(2));

            document.getElementById('totalPayment').value=(parseFloat(price_order)+parseFloat(price_distance)).toFixed(2);
            document.getElementById('delivery_fee').value=parseFloat(price_distance).toFixed(2);            
            
            document.getElementById('route_delivery_map').value=response.routes[0].legs[0].distance.value;

        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

    // Adds a marker to the map.
    function addMarker(location) {

        if(marker){
            marker.setMap();
        }
        

        marker = new google.maps.Marker({
          position: location,          
          //label: labels[labelIndex++ % labels.length],
          map: map,
          draggable: true,
          //animation: google.maps.Animation.DROP,
        });
        map.setCenter(location);

        google.maps.event.addListener(marker, 'dragend', function () {            
            map.setCenter(this.getPosition()); // Set map center to marker position            
            var pos = {
                lat: this.getPosition().lat(),
                lng: this.getPosition().lng()
            };
            //console.log(pos);
            geocodeLatLng(pos);
        });

        google.maps.event.addListener(map, "dragend", function() {                       
            marker.setPosition(this.getCenter());        
            
            var pos = {
                lat: this.getCenter().lat(),
                lng: this.getCenter().lng()
            };
            //console.log(pos);
            geocodeLatLng(pos);
        
        });

        google.maps.event.addListener(marker, 'click', function() {
        /*
              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                'Place ID: ' + place.place_id + '<br>' +
            place.formatted_address + '</div>');
              infowindow.open(map, this);
              */
        });


        geocodeLatLng(location);

      }



    function handleLocationError(browserHasGeolocation, infoWindow, pos) {

        
       /* infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);*/

      }

    function getLocation() {

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
                };
                addMarker(pos);

            }, function(error) {
                console.log('Get Location Error!!!');
                showError(error);
                //handleLocationError(true, infoWindow, map.getCenter());
            },{
                timeout:5000
            }
        );
        } else {
            console.log('Get Location Error!!!');
            // Browser doesn't support Geolocation
           // handleLocationError(false, infoWindow, map.getCenter());
        }
    }


    function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                console.log("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                console.log("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                console.log("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                console.log("An unknown error occurred.");
                break;
        }
    }


    var refreshIntervalId;



    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;

        console.log("start time");
        console.log(timer);

        //คำสั่งซื้อของคุณหมดอายุแล้ว คุณสามารถกลับไปทำรายการได้อีกครั้งที่หน้าอีเว้นท์
        if(new Date().getTime()<{{$cartItems->end_time*1000}}){
            refreshIntervalId= setInterval(function () {

                if(new Date().getTime()<{{$cartItems->end_time*1000}}){

                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);
                
                    minutes = minutes < 10 ? "0" + minutes : minutes; 
                    seconds = seconds < 10 ? "0" + seconds : seconds; 
                    var time__=--timer;
                    display.textContent=minutes + ":" + seconds; 
                    if (time__ < 0) { 
                        exitSession();
                    }
                }else{
                   exitSession();
                }

            }, 1000); 

        }else{
            exitSession();
        }
    }

    function exitSession(){
        alert("รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง"); 
        location.href="/cancel-order" ; 
        clearInterval(refreshIntervalId);
    }

    window.onhashchange = function() {
        //alert("5555");
    }

    

</script>

@endsection
