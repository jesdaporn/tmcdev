@extends('layouts.app_ryoii')

@section('content')
<style>


    .body-card-timer{padding:15px 35px;display: block;align-items:center;}
    .card-timer{font-size:32px;font-weight:bold;color:#3490dc;text-align:center;padding:15px;}
    .card-detail-timer{font-size:16px;color:#3490dc;padding:0px;padding-bottom:15px;}
    @media (min-width: 768px) {
        .body-card-timer{display: flex;align-items:center;}
        .card-timer{padding:0px;padding-right:35px;}
        .card-detail-timer{padding:0px;}
    }

    @media (min-width: 574px) {

    }

</style>
<div style="background-color: #fff;width:100%;height:100%;font-family: Tahoma, Geneva, Verdana, sans-serif;padding-top:35px;">

    <div class="container mb-4">
        <div class="d-flex justify-content-center">
            <div class="d-flex align-items-center">
                <span class="badge badge-pill badge-success">1</span>
                <div style="border-bottom:1px solid #38c172;width:100px;height:2px;margin-left:5px;margin-right:5px; "></div>
            </div>
            <div class="d-flex align-items-center">
                <span class="badge badge-pill badge-primary">2</span>
                <div style="border-bottom:1px dotted ;width:100px;height:2px;margin-left:5px;margin-right:5px; "></div>
            </div>
            <div class="d-flex align-items-center">
                <span class="badge badge-pill badge-secondary">3</span>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div style="width:130px;text-align:center;">
               <a  href="#" style="color:#000106; ">รายการสั่งซื้อ</a>
            </div>
            <div style="width:130px;text-align:center;">
                <a  href="#" style="color:#000106; ">ชำระเงิน</a>
            </div>
            <div style="width:130px;text-align:center;">
               <a  href="#" style="color:#6c757d;pointer-events: none;cursor: default; ">รายการสำเร็จ</a>
            </div>
        </div>

    </div>

    <div class="container mb-5">
        <div class="card">
            <div class="card-body" style="padding:0px;">
                <div class="body-card-timer">
                    <div class="card-timer" id="timer-countdown">
                        15:00
                    </div>
                    <div style="font-size:16px;color:#3490dc;">กรุณาทำรายการภายใน 15 นาที
                        หากท่านไม่ได้ทำรายการให้เสร็จภายในเวลาที่กำหนด
                        สิทธิ์การสั่งซื้อทั้งหมดจะถูกปล่อยให้ผู้ใช้งานท่านอื่น</div>
                </div>
            </div>
        </div>
    </div>


    <div class="container mb-5" style="margin-top:4rem;margin-bottom:1.5rem;">

        <?php
           // dump($OrderItems);
        ?>
    <form action="/payment-gateway" method="POST" >
        @csrf

        <input type="hidden" name="order_id" value="{{$OrderItems->order_id}}">

        <div class="row">
            <div class="col-md-8 col-sm-12">

                <div class="container mb-5 d-flex align-items-center">
                    <div style="width:250px;">
                        <img src="{{config('app.url_ryoiireview').$OrderItems->event_poster}}" class="img-fluid">
                    </div>
                    <div class="container" style="width:100%;">
                        <div style="font-size:19px;font-weight:bold;font-family:tahoma, Geneva, Verdana, sans-serif">
                            {{$OrderItems->event_name_th}}
                        </div>
                        <div style="font-size:12px;font-family:tahoma, Geneva, Verdana, sans-serif;font-weight:600;">
         

                        </div>

                    </div>
                </div>

                <h3 style="margin-bottom:1.5rem;font-size:20px;font-weight:600;">ช่องทางการชำระเงิน</h3>
                <div class="form-group shipping-couriers">                                          
                        <div class="card mb-2">
                            <div class="card-body p-0 d-flex">
                                <div class="" style="border-right:1px solid rgba(0, 0, 0, 0.125);padding:10px 15px;">
                                    <img src="/images/icon/i-creditcard.png" style="width:36px;">
                                </div>
                                <div class="form-group mb-0 d-flex align-items-center justify-content-between" style="padding:10px 15px;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input"  type="radio" name="paymentOption" id="paymentOption1" value="CC" required>
                                        <label class="form-check-label" for="paymentOption1">
                                            <h5 class="mb-0">บัตรเครดิต</h5>
                                        </label>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center ml-auto pr-2">
                                    <img src="/images/icon/i-visa.png" style="width:36px;margin-right:5px;">
                                    <img src="/images/icon/i-mastercard.png" style="width:36px;">
                                </div>

                            </div>
                        </div>

                        <div class="card mb-2">
                            <div class="card-body p-0 d-flex">
                                <div class="" style="border-right:1px solid rgba(0, 0, 0, 0.125);padding:10px 15px;">
                                    <img src="/images/icon/i-payment.png" style="width:36px;">
                                </div>
                                <div class="form-group mb-0 d-flex align-items-center" style="padding:10px 15px;">

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input"  type="radio" name="paymentOption" id="paymentOption2" value="BANK" required>
                                        <label class="form-check-label" for="paymentOption2">
                                            <h5 class="mb-0">โอนเงิน/ชำระผ่านธนาคาร</h5>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>                                           
                </div>

            </div>
            <div class="col-md-4 col-sm-12">
                <h3 style="margin-bottom:1.5rem;padding-left:10px;font-size:20px;font-weight:600;">
                     คำสั่งซื้อ {{$OrderItems->order_no}}
                </h3>
                <div class="card bg-light mb-3" style="border:none;">
                    <div class="card-header d-flex justify-content-between align-items-center" style="font-size:14px;border:none;">
                        <b>สรุปการจัดส่ง</b>                   
                    </div>
                    <div class="card-body" style="font-size:14px;">                        
                        <div style="font-weight: 400;">คุณ {{$OrderItems->address_delivery->firstname." ".$OrderItems->address_delivery->lastname}}</div>
                        <div>Email: {{$OrderItems->address_delivery->email}}</div>
                        <div>โทร: {{$OrderItems->address_delivery->tel}}</div>
                        <div></div>
                        <div>ที่อยู่: {{$OrderItems->address_delivery->address}}</div>
                        <div>
                            {{$OrderItems->address_delivery->district_name." ".$OrderItems->address_delivery->amphur_name." ".$OrderItems->address_delivery->province_name." ".$OrderItems->address_delivery->zipcode." "}}
                        </div>
                    </diiv>
                </div>
                <div class="card bg-light mb-3" style="border:none;">
                    <div class="card-header" style="font-size:14px;border:none;"><b>สรุปคำสั่งซื้อ</b></div>
                    <div class="card-body" style="font-size:12px;font-weight:600;">
                        @foreach ($OrderItems->order_detail as $item)
                            <div class="d-flex justify-content-between">
                               <div>{{$item->menu_name_th}}</div>
                                <div>{{$item->qty_unit}} x {{number_format($item->price_per_unit,2)}}</div>
                            </div>
                            @foreach ($item->order_option as $otp)
                                <div class="d-flex justify-content-between" style="font-size:12px;font-weight:normal;">
                                    <div style="padding-left:15px;">+{{$otp->option_name}}</div>
                                    <div>{{$otp->qty_unit}} x {{number_format($otp->option_price,2)}}</div>
                                </div>
                            @endforeach
                            
                        @endforeach
                    </div>
                </div>
                <div class="card bg-light mb-3" style="border:none;">
                    <div class="card-header" style="font-size:14px;border:none;"><b>สรุปยอดชำระ</b></div>
                    <div class="card-body" style="font-size:12px;font-weight:600;">
                        <div class="d-flex justify-content-between">
                            <div>ค่าสินค้า</div>
                            <div>{{number_format($OrderItems->amount,2)}} บาท</div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>ค่าขนส่ง</div>
                            <div>{{number_format($OrderItems->delivery_fee,2)}} บาท</div>
                        </div>
                    </div>
                    <div class="card-footer bg-white d-flex justify-content-between" style="font-size:14px;">
                        <div style="font-weight: bold;">ยอดชำระสุทธิ</div>
                        <div style="text-align: right;font-weight: bold;">{{number_format($OrderItems->payment_amount,2)}} บาท</div>
                    </div>
                    
                </div>
            </div>

            <div class="col-12 text-center mt-5 mb-5">
                <button class="btn btn-success" style="width:200px;" type="submit">ชำระตอนนี้</button>
            </div>

        
        </div>
    </form>

    
    </div>
</div>


<script>
    setTimeout(() => {
        $(document).ready(function () {

        });

    }, 500);


    var el_redio = document.getElementsByName('inlineRadioOptions');
    for (var i = 0; i < el_redio.length; i++) {
        el_redio[i].addEventListener('change',function(){
            //document.getElementsByName('payment_option').value=this.value;
        });

    }

    var refreshIntervalId;

    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        
        //คำสั่งซื้อของคุณหมดอายุแล้ว คุณสามารถกลับไปทำรายการได้อีกครั้งที่หน้าอีเว้นท์
        if(new Date().getTime()<{{$OrderItems->end_time*1000}}){ 
            refreshIntervalId=setInterval(function () { 
                if(new Date().getTime()<{{$OrderItems->end_time*1000}}){ 
                    minutes=parseInt(timer / 60, 10); 
                    seconds=parseInt(timer % 60, 10);
            
                    minutes=minutes < 10 ? "0" + minutes : minutes; 
                    seconds=seconds < 10 ? "0" + seconds : seconds; 
                    var time__=--timer;
                    display.textContent=minutes + ":" + seconds; 

                    if (time__ < 0) { 
                        exitSession();                       
                    } 
                }else{
                    if (time__ < 0) { 
                        exitSession();                       
                    }
                   exitSession();
                } 
            }, 1000); 
        }else{
            exitSession();
        } 
    } 

    function exitSession(){
        alert("รายการสั่งซื้อของคุณหมดอายุ คุณสามารถกลับไปทำรายการใหม่ได้อีกครั้ง"); 
        location.href="/cancel-order" ; 
        clearInterval(refreshIntervalId);
    }


    window.onload=function () { 
        var display=document.querySelector('#timer-countdown'); 
        
        @if($OrderItems->checkPeriod)
        //========== between start time to end time
        var period_time ={{$OrderItems->end_time*1000}}-new Date().getTime()

        //time(seconds)
        var NumberMinutes = period_time/1000;

        startTimer(NumberMinutes, display);
        @else
           exitSession();
        @endif

    
    };

</script>
@endsection
