@extends('layouts.app_ryoii')

@section('content')
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@600&display=swap" rel="stylesheet">
<style>

    body {
        font-size: 16px;
        line-height: 1.5;
        color: #000106;
        background-color: #fff;
    }

    .bg-header {
        position: absolute;
        left: -30px;
        right: -30px;
        top: -30px;
        bottom: -30px;
        background-repeat: no-repeat;
        filter: blur(15px);
        background-size: cover;
        background-position: center;
    }

    .panel-header {
        background-color: rgba(255, 222, 40, 0.8);
        color: rgb(0, 1, 6);
        display: block;
    }

    .image-header {
        max-width: 100%;
    }

    .font-head {
        font-family: inherit;
        font-weight: bold;
        line-height: 1.5;
        color: #000106;
    }

    .text-detail-payment {
        font-family: Sarabun, system-ui, -apple-system, san-serif;
        font-size: 16px;
        line-height: 1.5;
        color: #000106;
    }

    .set-packet {
        margin-bottom: 74px;
    }

    .justify-content-resize {
        margin-top: 5px;
        justify-content: space-between;
    }

    .body-card-order {
        padding: 15px;
    }

    .container-select-order {
        border-bottom: 1px solid #e5e5e6;
        padding: 15px 0px;
    }

    .d-flex-align-items-center {
        display: block;
    }

    .btn-buyer-default {
        width: 100% !important;
    }

    .btn-buyer {
        background-color: #000106;
        border-color: #000106;
        box-shadow: none;
        color: #fff;
        opacity: 0.3333333333333333;    
        white-space: nowrap;
        padding: 6px 24px;
        font-size: 16px;
        line-height: 1.5;
        border-radius: 3px;
    }

    .text-buyer {
        color: #b2b2b4;
        font-size: 16px;
        font-weight: normal;
        margin-bottom: 10px;
    }


    .container-head-body{padding:36px 26px !important;}

    .font-title-package{font-size: 19px !important;}
    .font-desc-package{color:#260d31;font-size:14px;font-weight:normal;}

    .header-package{
        background-color: #eee;border-bottom: 1px solid #f5f5f6;margin-bottom: 0;padding: 11px 16px;cursor:pointer;
    }

    .detail_post img{
        max-width:100%;
        width:100% !important;
    }

    .detail_post iframe{
        max-width:100%;
    }
    
    .form-check .input-group{
        width:56px;
        margin-left:8px;
    }

    .form-check .input-group .btn{
        min-width: 15px !important;
        height: 25px;
        padding:0px;
    }

    .form-check .input-group input{
    min-width: 15px !important;
    height: 25px;
    padding:0px;
    font-size: 12px;
    }

    .price-txt-option{margin-right:0px;}

   
    .channelPayment{text-align: center;}

    @media (min-width: 768px) {

        .channelPayment{text-align: left;}

        .price-txt-option{margin-right:55px;}
        .detail_post img{
            width:auto !important;
        }

        .panel-header {
            display: flex;
        }

        .image-header {
            max-width: 32%;
        }

        .justify-content-resize {
            margin-top: 0px;
            justify-content: flex-end;
        }

        .body-card-order {
            padding: 0px 35px 35px 35px;
        }

        .container-select-order {
            border-bottom: 1px solid #e5e5e6;
            padding: 35px 0px;
        }

        .d-flex-align-items-center {
            display: flex;
            align-items: center;
        }

        .btn-buyer-default {
            width: 200px !important;
        }

        .text-buyer {
            color: #b2b2b4;
            font-size: 16px;
            font-weight: 600;
            margin-bottom: 0px;
        }
    }

    @media (min-width: 574px) {
        .container-head-body{padding:36px 63px !important;}
        .font-title-package{font-size: 26px !important;}
        .font-desc-package{font-weight:bold;}
    }

</style>
<div style="background-color: #fff;width:100%;height:100%;">

    <?php
        //dump($event);
    ?>

    <!-- Head Title --> 
    <div style="background-position: top center;background-repeat: no-repeat;background-size: cover;position: relative;">
        <div style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;overflow: hidden;">
        <div class="bg-header" style="background-image: url('{{config('app.url_ryoiireview').$event->event_bg}}');">

            </div>
        </div>
            <!--/*padding:48px 15px;-->
        <div class="container" style="max-width:100%;position:relative;padding:0px;margin:auto;">
            <div style="width:100%;text-align:center;">
                <img src="{{config('app.url_ryoiireview').$event->event_poster}}" class="img-fluid" style="width:100%">
            </div>
            <div class="panel-header d-none">
                <div class="image-header">
                    <img src="{{config('app.url_ryoiireview').$event->event_poster}}" class="img-fluid">
                </div>
                <div class="d-flex align-items-center">
                    <div class="container container-head-body" style="width:100%;">
                        <div style="font-size:16px;font-family:sukhumvitset-medium;font-weight:600;">
                            Shabu 
                        </div>
                        <h1 style="font-size:28px;font-weight:bold;margin-bottom:1.5rem;font-family:tahoma, Geneva, Verdana, sans-serif">
                            {{$event->event_name_th}}
                        </h1>

                    </div>
                </div>

            </div>
        </div>
    </div><!-- End Head Title -->


    <div class="container mb-1" style="padding:40px 15px;max-width:978px;">

        <!-- Detail Package -->
        <div style="margin-bottom:56px;" class="detail_post">
            {!!$event->detail!!}

        </div><!-- End Detail Package -->


        <!-- Tags Package -->
        <!--
        <div class="text-detail-payment" style="text-align:left;margin-bottom:96px;">
            <i class="fa fa-tag"></i>
            <span style="margin-left:10px;">#shabu, #shabuthailand, #penguineatshabu</span>
        </div>-->
        <!-- End Tags Package -->

        <!-- Select Package -->
        <div style="text-align:left;">

            <h2 style="margin-bottom:16px;font-family:inherit;font-size:28px;font-weight:bold;line-height:1.5;margin-bottom:0px;">เลือกแพคเกจ</h2>
            <h4 class="font-head font-title-package" style="color:#e3342f;margin-bottom:15px;font-size:14px;">จัดส่งในวัน เสาร์ที่ 25/07/2020</h4>
            <form method="POST" action="/add-to-cart"  onsubmit="return onSubmitFormOrder()">
                @csrf
            <input type="hidden" name="event_id" value="{{$event->event_id}}">
                @foreach($event->catesetmenu as $cate_menu)
                    @if(count($cate_menu->setmenu)>0)
                    <div class="card mb-4">

                        <div class="card-header header-package" id="heading_{{$cate_menu->cate_id}}">
                            <div data-toggle="collapse" data-target="#collapse_{{$cate_menu->cate_id}}" aria-expanded="true" aria-controls="collapse_{{$cate_menu->cate_id}}"
                                class="d-flex">
                                <div>
                                <h3 class="font-head font-title-package" style="margin-bottom:0px;color:#000106;">{{$cate_menu->cate_name}}</h3>
                                    <small class="text-muted font-desc-package">จัดส่งทั่วกรุงเทพและปริมณฑล</small>
                                </div>
                                <div class="d-flex align-items-center" style="margin-left:auto;font-size:24px;font-weight:bold;">
                                    <i class="fa fa-caret-down separator-icon"></i>
                                </div>                        
                            </div>                        
                        </div>


                        <div id="collapse_{{$cate_menu->cate_id}}" class="collapse show">
                            <div class="card-body body-card-order">

                                @foreach($cate_menu->setmenu as $menu)
                                    <div class="container container-select-order">
                                        <div class="row">

                                            <div class="col-md-6 col-sm-12">
                                                <div class="detail-name ticket-name">
                                                <div style="font-size:18px;font-weight:bold;">{{$menu->menu_name_th}}</div>
                                                    <small class="mt-1" style="font-size:14px;color:#260d31;">
                                                        {{$menu->menu_desc}}
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="d-flex justify-content-resize align-items-center">
                                                    <div style="font-size:18px;color:#260d31;">฿{{number_format($menu->price,2)}}</div>
                                                    <input type="hidden" value="{{$menu->set_menu_id}}" name="menu_id[]">
                                                    <input type="hidden" value="{{$menu->price}}" name="price_menu_id_{{$menu->set_menu_id}}">
                                                    <select style="width:65px;margin-left:55px;" class="form-control" data-id="{{$menu->set_menu_id}}" data-price="{{$menu->price}}"
                                                        data-menu="{{$menu->menu_name_th}}" name="qty_menu_id_{{$menu->set_menu_id}}">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>

                                            </div>

                                            @if(count($menu->option_menu)>0)
                                               <div class="col-12">
                                                    <div class="mt-2">
                                                        <div class="d-flex" data-toggle="collapse" data-target="#collapse_set_{{$menu->set_menu_id}}" aria-expanded="true" aria-controls="collapse_set_{{$menu->set_menu_id}}">
                                                            <h5 style="font-size:14px;font-weight:600;margin-bottom:0px;line-height:1.6;">เลือกเมนูพิ่มเติม </h5>
                                                            <div class="ml-2"><i class="fas fa-plus-circle"></i></div>
                                                        </div>
                                                        
                                                                                                                     
                                                            @foreach($menu->option_menu as $opk=>$opv)
                                                        <div id="collapse_set_{{$menu->set_menu_id}}" class="collapse show">
                                                            <div class="p-1 d-flex justify-content-between" style="line-height: 1;">
                                                                <div class="form-group form-check" style="font-size:14px;margin-bottom:0px;display:flex;align-items:center;">
                                                                    <!--<input type="checkbox" style="margin-top:0px;" class="form-check-input" id="option_id_"  name="options_menu1[]" value="">-->
                                                                    <label style="min-width:100px;" class="form-check-label" for="option_id_{{$opv->option_id}}">-{{$opv->option_name}}</label>
                                                                    
                                                                </div>
                                                                
                                                                <div class="form-check d-flex ml-auto align-items-center" style="color:#707070;font-size:14px;">
                                                                    <span class="price-txt-option">+฿{{number_format($opv->option_price,2)}}</span>
                                                                    <input type="number" value="0" min="0" max="10" step="1" name="num_option_{{$opv->option_id}}" />
                                                                </div>
                                                             
                                                            </div>
                                                        </div>
                                                            
                                                            @endforeach
                                                                                                              
                                                    </div>                            
                                                </div>                                               
                                            @endif

                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                    </div>
                    @endif
                @endforeach

            <div class="d-flex-align-items-center mb-4">
                <div class="ml-auto">
                    <button class="btn-buyer btn-buyer-default" type="submit" id="btn_submit_form" disabled style="min-width:200px;">สั่งซื้อ</button>
                </div>
            </div>
            </form>

            <!-- Start Channel Payment -->
            <div class="channelPayment">
                <div style="font-size:14px;font-weight:bold;line-height:1.5;margin-bottom:16px;">
                    ช่องทางการชำระเงินที่รองรับ</div>
                <div class="mb-2">
                    <img height="46"  class="mr-3"
                        src="/images/icon/i-visa.png"
                        alt="Visa icon">
                    <img height="46" class="mr-3"
                        src="/images/icon/i-mastercard.png"
                        alt="Master icon">

                    <img height="46" class="mr-3" src="/images/icon/i-barcode.png" alt="ชำระเงินผ่านบิล">

                </div>
            </div>
            <!-- End Channel Payment -->

        </div>

    </div>



</div>
<script>
    setTimeout(() => {
        $(document).ready(function () {
            

            document.getElementById("btn_submit_form").disabled = true;


            $("input[type='number']").inputSpinner().val(0);
            $('select').val(0);

            $('.header-package').click(function() {

                //console.log($(this).children(":first").attr('aria-expanded'));
               if($(this).children(":first").attr('aria-expanded')=="true"){
                        $(this).children()[0].children[1].innerHTML='<i class="fa fa-caret-right separator-icon"></i>';
                }else{
                    $(this).children()[0].children[1].innerHTML='<i class="fa fa-caret-down separator-icon"></i>';
                }
            });

        });
    }, 500);

    var inputElem=document.getElementsByTagName("select");
    for(let i = 0; i < inputElem.length; i++) { 
        inputElem[i].addEventListener('change', function(){ 
            let btn_submit = document.getElementById("btn_submit_form");
            console.log(this.value);
            if(checkValueForm()){                
                btn_submit.classList.remove('btn-buyer');
                btn_submit.classList.add('btn');
                btn_submit.classList.add('btn-success');
                document.getElementById("btn_submit_form").disabled = false;
            }else{
                btn_submit.classList.remove('btn')
                btn_submit.classList.remove('btn-success')
                btn_submit.classList.add('btn-buyer');
                document.getElementById("btn_submit_form").disabled = true;
            }

        },false); 
    }

    var checkValueForm=()=>{
        let OrderSelect=[];
        let dataSelect = document.querySelectorAll("form select");
       
        let status_btn = false;
        dataSelect.forEach((v,k)=>{
            
            if(v.value>0){
         
                OrderSelect.push({
                    id:v.dataset.id,
                    price:v.dataset.price,
                    menu:v.dataset.menu,
                    num:v.value
                });
                
                status_btn = true;
            }
            
        });
        console.log(OrderSelect);
        return status_btn;
    }
    

     var onSubmitFormOrder=()=>{

        if(checkValueForm()){
            return;
        }
       
        return false;

    }


</script>
@endsection
