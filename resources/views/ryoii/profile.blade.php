@extends('layouts.app_ryoii')



@section('content')

<style>
    .card-title {
        font-weight: 600;
    }
</style>




<div class="container" >
    <h3 class="pl-2 mt-5 mb-3" style="font-weight: bold;">ข้อมูลบัญชีผู้ใช้</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                aria-selected="true"><b>ข้อมูลส่วนตัว</b></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="address-tab" data-toggle="tab" href="#address" role="tab" aria-controls="address"
                aria-selected="false"><b>จัดการที่อยู่จัดส่ง</b></a>
        </li>

    </ul>
   
    <div class="tab-content container">
        <div class="tab-pane active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <form class="mt-3" action="/profile" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div>
                            <label for="inputEmail4">รูปโปรไฟล์</label>
                            <input type="hidden" name="profile_id" value="{{$profile->id}}" />
                            <div style="width:65px;height:65px;">
                            <img src="{{asset('storage')}}/{{$profile->image_icon}}" style="width:100%;">
                            <input type="hidden" name="image_icon" value="{{$profile->image_icon}}" />
                            </div>
                            <div class="custom-file mt-2">
                                <input type="file" class="custom-file-input" id="profile_image" name="profile_image">                                
                                <label class="custom-file-label" for="profile_image">Choose file</label>                                
                            </div>
                            @error('profile_image')
                                <div class="invalid-feedback" style="display: block;">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputEmail4">ชื่อ(นามสมมติ)</label>
                        <input type="text" class="form-control @error('inp_name') is-invalid @enderror" id="inp_name" name="inp_name" value="{{$profile->name}}">
                        @error('inp_name')
                            <div class="invalid-feedback" style="display: block;">{{$message}}</div>
                        @enderror
                        
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">ชื่อ</label>
                        <input type="text" class="form-control @error('inp_firstname') is-invalid @enderror" id="inp_firstname" name="inp_firstname" value="{{$profile->firstname}}">
                        @error('inp_firstname')
                            <div class="invalid-feedback" style="display: block;">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">นามสกุล</label>
                        <input type="text" class="form-control @error('inp_lastname') is-invalid @enderror" id="inp_lastname" name="inp_lastname" value="{{$profile->lastname}}">
                        @error('inp_lastname')
                            <div class="invalid-feedback" style="display: block;">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inp_email">อีเมล</label>
                        <input type="email" class="form-control" id="inp_email" name="inp_email" value="{{$profile->email}}" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inp_tel">เบอร์ติดต่อ</label>
                        <input type="tel" class="form-control @error('inp_tel') is-invalid @enderror" id="inp_tel" name="inp_tel" value="{{$profile->tel}}">
                        @error('inp_tel')
                            <div class="invalid-feedback" style="display: block;">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-groub mb-2">
                    <div for="sex" style="margin-bottom:0px;">เพศ</div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex" id="sex1" value="1" {{$profile->sex=="1"?"checked":""}}>
                        <label class="form-check-label" for="sex1">ชาย</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex"  id="sex2" value="2" {{$profile->sex=="2"?"checked":""}}>
                        <label class="form-check-label" for="sex2">หญิง</label>
                    </div>
                </div>
                <!--<div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">วันที่</label>
                        <select id="inputState" class="form-control">
                            <option selected>Choose...</option>
                            <option>...</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">เดือน</label>
                        <select id="inputState" class="form-control">
                            <option selected>Choose...</option>
                            <option>...</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">ปี(พ.ศ.) </label>
                        <input type="text" class="form-control" id="inputZip">
                    </div>
                </div>-->
                <div class="text-right mt-5">
                    <button type="submit" class="btn btn-success">บันทึกการเปลี่ยนแปลง</button>
                </div>
                
            </form>
        </div>
        <div class="tab-pane" id="address" role="tabpanel" aria-labelledby="address-tab">
            <div class="container mt-3">
                <div class="card mb-3">
                    <div class="card-body d-flex justify-content-between">
                        <div class="pr-5">
                            <div>เจษฎาพร มั่งมี</div>
                            89 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400 อาคาร AIA Capitalcenter ถนน ดินแดง
                            ดินแดง / Dindang ดินแดง / Din Daeng กรุงเทพมหานคร / Bangkok
                            10400
                        </div>
                        <div class="ml-auto d-flex align-items-center">
                            <button class="btn btn-primary btn-sm mr-2">แก้ไข</button>
                            <button class="btn btn-danger btn-sm">ลบ</button>
                        </div>
                    </div>
                </div>


                <div class="card mb-3">
                    <div class="card-body d-flex justify-content-between">
                        <div class="pr-5">
                            <div>เจษฎาพร มั่งมี</div>
                            89 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400 อาคาร AIA Capitalcenter ถนน ดินแดง
                            ดินแดง / Dindang ดินแดง / Din Daeng กรุงเทพมหานคร / Bangkok
                            10400
                        </div>
                        <div class="ml-auto d-flex align-items-center">
                            <button class="btn btn-primary btn-sm mr-2">แก้ไข</button>
                            <button class="btn btn-danger btn-sm">ลบ</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>
@endsection
