
@extends('layouts.app_ryoii')

@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center" style="font-weight: bold;">สถานะชำระเงิน</div>                
                <div class="card-body">
                    @if($payment->payment_status_code=="000")
                        <div>
                            ชำระเงินสำเร็จ ระบบกำลังกลับไปหน้าเว็บปกติ...
                        </div>
                    @else
                        <div style="color:red;">
                            การชำระเงินไม่สำเร็จ ระบบกำลังกลับไปหน้าเว็บปกติ...                            
                        </div>
                        <div style="color:yello;">{{$payment->channel_response_desc}}</div>
                    @endif    
                    <p class="mt-2 text-right">
                        <a href="/">หน้าหลัก</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
