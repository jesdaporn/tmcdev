@extends('layouts.app_ryoii')



@section('content')

<style>
    .card-title {
        font-weight: 600;
    }
    .container-bg-search{
        width:100%;
        padding:2.5rem 0.5rem;
        background-image:url('/images/banner/container_track.jpg');
        background-repeat:no-repeat;
        background-position:center;
        background-size:cover;
    }
    .box-search-order{width:82% !important;padding: 0.375rem 0.75rem;}

    .btn-search-order{background-color:#de231a;border:none;width:15%;height:36px;font-size:19px;}
    .txt-search{display: none;}
    @media (min-width: 768px) {
        .box-search-order{padding:28px;}
        .container-bg-search{
            padding:2.5rem 1rem;
        }
        .txt-search{display: inline;}
        .btn-search-order{width:15%;height:56px;font-size:19px;}
    }
</style>




<div class="container" style="max-width: 100%;padding:0px;">

    <div class="mb-5 container-bg-search">
        <div class="container" style="width: 100%;padding:0px;">
                <h3 class="pl-2 " style="font-weight: bold;color:#fff;">ติดตามรายการสั่งซื้อ</h3>            
                <div class="mb-5">
                    <form class="form-inline" action="/track-order" method="POST">
                        @csrf
                        <input type="text" class="form-control mb-2 mr-sm-2 box-search-order" required name="inp_search_order"
                            placeholder="Search for email or order no." >
                        <button type="submit" class="btn btn-primary mb-2 btn-search-order">
                            <i class="fa fa-search" style="font-size:16px;"></i> <span class="txt-search">ค้นหา</span></button>
                    </form>
                </div>
            </div>
    </div>

 

    <div class="container pt-5 pb-5">
        @if($dataOrder)
        
        <div class="row">
            <div class="col-md-12">
                @foreach($dataOrder as $k=>$v)
                    <div class="card mb-5" style="width:100%;max-width:35rem;margin:auto;">
                        <div class="card-header p-3">
                        <h5 class="card-title mb-1">คำสั่งซื้อ {{$v->order_no}}</h5>
                            <div class="d-flex" style="font-size:12px;color:#5a5c69;line-height:1.4;">
                                <div>
                                    <div>สั่งซื้อวันที่ {{$v->order_date}} {{$v->order_time}}</div>
                                    <div>ชำระเงินเมื่อ {{$v->update_dtm}}</div>
                                </div>
                                <div class="ml-auto" style="font-weight:600;">
                                    <div>
                                        @if($v->payment_status_code=="000")
                                           ชำระเงินแล้ว
                                        @elseif($v->payment_status_code=="001")
                                            รอการชำระเงิน
                                        @elseif($v->payment_status_code=="002")
                                            การชำระเงินถูกปฏิเสธ
                                        @elseif($v->payment_status_code=="003")                                            
                                            การชำระเงินถูกยกเลิกโดยผู้ใช้
                                        @elseif($v->payment_status_code=="999")                                            
                                            การชำระเงินล้มเหลว
                                        @else
                                            อยู่ระหว่างการตรวจสอบ
                                        @endif
                                        </div>
                                    <div>
                                      @if($v->payment_channel_code=="001")
                                            Credit cards
                                        @elseif($v->payment_channel_code=="002")
                                            จ่ายเงินสด
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-body p-3">
                            <div class="mb-2">
                                <div style="font-weight:600;font-size:16px;margin-bottom:10px;">Orangii X Ryoii อิ่มอร่อยกับ
                                    ชาบูแถมหม้อหุงข้าวอัจฉริยะ</div>

                                    @if($v->order_detail)
                                        @foreach($v->order_detail as $k1=>$v1)
                                            <div class="d-flex pl-2" style="font-weight: 600;font-size:12px;">
                                                <div>{{$v1->menu_name_th}}</div>
                                                <div class="ml-auto">{{$v1->qty_unit}} x {{number_format($v1->price_per_unit,2)}}</div>
                                            </div>

                                            @if($v1->order_option)
                                                @foreach($v1->order_option as $k3=>$v3)
                                                <div class="d-flex pl-2" style="font-size:11px;">
                                                    <div class="pl-2">{{$v3->option_name}}</div>
                                                    <div class="ml-auto">{{$v3->qty_unit}} x {{number_format($v3->price_per_unit,2)}}</div>
                                                </div>
                                                @endforeach
                                            @endif
                                        
                                        @endforeach
                                    @endif
                                
                                


                            </div>
                            <div class="card-text text-right" style="color:#df0010;font-weight:600;font-size:12px;">
                                ค่าจัดส่ง {{$v->delivery_fee}}                                    
                            </div>
                            <p class="card-text text-right" style="font-weight: 600;"> รวมทั้งสิ้น: <span
                            style="color:#df0010;font-weight:600;">฿{{number_format($v->payment_amount,2)}}</span></p>

                        </div>
                    </div>
                @endforeach
                
            </div>

        </div>
        @else
            <h3 class="text-center">ไม่พบรายการที่ค้นหา</h3>
        @endif
        
    </div>
    
    

</div>
@endsection
