@extends('layouts.app_ryoii')



@section('content')

<style>
    .card-title {
        font-weight: 600;
    }
</style>




<div class="container" >
    <h3 class="pl-2 mt-5 mb-3" style="font-weight: bold;">รายการคำสั่งซื้อ</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="alltransaction-tab" data-toggle="tab" href="#alltransaction" role="tab" aria-controls="alltransaction"
                aria-selected="true"><b>รายการทั้งหมด</b></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="transaction1-tab" data-toggle="tab" href="#transaction1" role="tab" aria-controls="transaction1"
                aria-selected="false"><b>รายการที่ชำระแล้ว</b></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="transaction2-tab" data-toggle="tab" href="#transaction2" role="tab" aria-controls="transaction2"
                aria-selected="false"><b>รายการที่รอจัดส่ง</b></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="transaction3-tab" data-toggle="tab" href="#transaction3" role="tab" aria-controls="transaction3"
                aria-selected="false"><b>รายการที่สำเร็จ</b></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="transaction4-tab" data-toggle="tab" href="#transaction4" role="tab" aria-controls="transaction4"
                aria-selected="false"><b>รายการคืนเงิน</b></a>
        </li>

    </ul>

    <div class="tab-content container">
        <div class="tab-pane active pt-2" id="alltransaction" role="tabpanel" aria-labelledby="alltransaction-tab">

         
            <div class="card mb-2" style="border-right:5px solid #38c172;cursor:pointer;">
                <div class="card-body p-2" style="color:#5a5c69;">
                <div class="media">
                    <img src="/images/img_payment1.jpg" class="mr-3" alt="" style="height:65px;width:78px;">
                    <div class="media-body">
                        <div class="d-flex justify-content-between align-items-center">                            
                            <div>
                                <h5 class="mb-2" style="font-weight:600;font-size:14px;">Penguin Eat Shabu - Next Day</h5>

                                <div class="card-text" style="font-size:11px;">
                                    <i class="far fa-comment-alt"></i>
                                    Penguin Eat Shabu - เพนกวินกินชาบู
                                    ความสุขของกวิ้น คือได้เห็นทุกคนมีความสุขในการกิน
                                </div>
                                <div style="font-size:12px;"><i class="far fa-clock"></i>23/06/2020 14:00</div>
                            </div>

                            <div class="d-flex">
                                <span class="badge badge-success" style="height:16px;margin-right:5px;margin-top:2px;">สำเร็จ</span>                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                </div>
            </div>

            <div class="card mb-2" style="border-right:5px solid #3490dc;cursor:pointer;">
                <div class="card-body p-2" style="color:#5a5c69;">
                    <div class="media">
                        <img src="/images/img_payment1.jpg" class="mr-3" alt="" style="height:65px;width:78px;">
                        <div class="media-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-2" style="font-weight:600;font-size:14px;">Penguin Eat Shabu - Next Day</h5>
            
                                    <div class="card-text" style="font-size:11px;">
                                        <i class="far fa-comment-alt"></i>
                                        Penguin Eat Shabu - เพนกวินกินชาบู
                                        ความสุขของกวิ้น คือได้เห็นทุกคนมีความสุขในการกิน
                                    </div>
                                    <div style="font-size:12px;"><i class="far fa-clock"></i>23/06/2020 14:00</div>
                                </div>
            
                                <div class="d-flex">
                                    <span class="badge badge-primary"
                                        style="height:16px;margin-right:5px;margin-top:2px;">ชำระ</span>
                                </div>
                            </div>
            
                        </div>
                    </div>
                </div>
            </div>
           

        </div>

        <div class="tab-pane pt-2" id="transaction1" role="tabpanel" aria-labelledby="transaction1-tab">
            <div class="card mb-2" style="border-right:5px solid #3490dc;cursor:pointer;">
                <div class="card-body p-2" style="color:#5a5c69;">
                    <div class="media">
                        <img src="/images/img_payment1.jpg" class="mr-3" alt="" style="height:65px;width:78px;">
                        <div class="media-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-2" style="font-weight:600;font-size:14px;">Penguin Eat Shabu - Next Day</h5>
            
                                    <div class="card-text" style="font-size:11px;">
                                        <i class="far fa-comment-alt"></i>
                                        Penguin Eat Shabu - เพนกวินกินชาบู
                                        ความสุขของกวิ้น คือได้เห็นทุกคนมีความสุขในการกิน
                                    </div>
                                    <div style="font-size:12px;"><i class="far fa-clock"></i>23/06/2020 14:00</div>
                                </div>
            
                                <div class="d-flex">
                                    <span class="badge badge-primary"
                                        style="height:16px;margin-right:5px;margin-top:2px;">ชำระ</span>
                                </div>
                            </div>
            
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane pt-2" id="transaction2" role="tabpanel" aria-labelledby="transaction2-tab">
           
        </div>

        <div class="tab-pane pt-2" id="transaction3" role="tabpanel" aria-labelledby="transaction3-tab">
            <div class="card mb-2" style="border-right:5px solid #38c172;cursor:pointer;">
                <div class="card-body p-2" style="color:#5a5c69;">
                    <div class="media">
                        <img src="/images/img_payment1.jpg" class="mr-3" alt="" style="height:65px;width:78px;">
                        <div class="media-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-2" style="font-weight:600;font-size:14px;">Penguin Eat Shabu - Next Day</h5>
        
                                    <div class="card-text" style="font-size:11px;">
                                        <i class="far fa-comment-alt"></i>
                                        Penguin Eat Shabu - เพนกวินกินชาบู
                                        ความสุขของกวิ้น คือได้เห็นทุกคนมีความสุขในการกิน
                                    </div>
                                    <div style="font-size:12px;"><i class="far fa-clock"></i>23/06/2020 14:00</div>
                                </div>
        
                                <div class="d-flex">
                                    <span class="badge badge-success"
                                        style="height:16px;margin-right:5px;margin-top:2px;">สำเร็จ</span>
                                </div>
                            </div>
        
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane pt-2" id="transaction4" role="tabpanel" aria-labelledby="transaction4-tab">
            
        </div>

    </div>



</div>
@endsection
