<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Route::get('/','RyoiiController@index')->name('home-page');
//Route::get('home', 'RyoiiController@index')->name('home');
Route::view('/policy','ryoii.policy');
//Auth::routes();

Route::get('/test', function () {
    $date = new DateTime();
    $timeZone = $date->getTimezone();
    echo $timeZone->getName();
    echo date('Y-m-d H:i:s');
});

/***** Start Route Member  *****/
/*
Route::post('/member-register', 'MemberRyoiiController@register')->name('member-register');
Route::post('/member-login', 'MemberRyoiiController@login')->name('member-login');
Route::get('/member-logout', 'MemberRyoiiController@logout')->name('member-logout');
Route::post('/member-reset-password', 'MemberRyoiiController@resetpassword')->name('member-reset-password');
Route::post('/social-login', 'MemberRyoiiController@socialLogin')->name('social-login');
*/

/***** End Route Member  *****/

/*
Route::get('/profile', 'RyoiiController@profile')->name('profile');
Route::post('/profile', 'RyoiiController@updateProfile')->name('update-profile');
Route::get('/transactions', 'RyoiiController@transactions')->name('transactions');
*/

Route::get('/test-mail', 'RyoiiController@testmail')->name('test-mail');

Route::get('/track-order', 'RyoiiController@trackorder')->name('track-order');
Route::post('/track-order', 'RyoiiController@trackorder')->name('search-order');

Route::get('/post/{url}', 'RyoiiController@post')->name('post');
Route::get('/result-payment', 'RyoiiController@resultpayment')->name('result-payment');
Route::post('/result-payment', 'RyoiiController@resultpayment')->name('result-payment');
Route::put('/result-payment', 'RyoiiController@resultpayment')->name('result-payment');
Route::delete('/result-payment', 'RyoiiController@resultpayment')->name('result-payment');



Route::post('/add-to-cart', 'RyoiiController@addProductToCart')->name('addProduct-toCart');
Route::get('/confirm-order/{cart_id}', 'RyoiiController@confirmorder')->name('confirm-order');
Route::post('/order-payment', 'RyoiiController@orderpayment')->name('order-payment');
Route::get('/show-payment-gateway/{order_id}', 'RyoiiController@showPaymentGayway')->name('show-payment');


Route::get('/cancel-order', 'RyoiiController@cancelOrder')->name('cancel-order');
Route::post('/payment-gateway', 'RyoiiController@paymentGateway')->name('payment-gateway');
Route::get('/admin-restaurant', 'AdminRestaurantController@index')->name('admin-restaurant');



/*======== Service get Address ==============*/

use Illuminate\Support\Str;
Route::get('/generateOrderNumber ', function () {

    $num = DB::table('ev_orders')->count()+1;

    $today  = date('Ymd');
    $order_no = $today.'-'. str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT).$num;    

    return response()->json($order_no);
});

Route::get('/getProvince', function () {
    //$result = DB::table('ar_province')->get();

    $result = array(
            array(
            "province_id" => 1,
            "province_code" => 10,
            "province_name" => "กรุงเทพมหานคร ",
            "country_id" => 218
            ), array(
            "province_id" => 2,
            "province_code" => 11,
            "province_name" => "สมุทรปราการ ",
            "country_id" => 218
            ), array(
            "province_id" => 3,
            "province_code" => 12,
            "province_name" => "นนทบุรี ",
            "country_id" => 218
            ), array(
            "province_id" => 4,
            "province_code" => 13,
            "province_name" => "ปทุมธานี ",
            "country_id" => 218
        )
        
    );
    
    /*$result =array(        
        {province_id: 1, province_code: "10", province_name: "กรุงเทพมหานคร ", country_id: 218},
        {province_id: 2, province_code: "11", province_name: "สมุทรปราการ ", country_id: 218},
        {province_id: 3, province_code: "12", province_name: "นนทบุรี ", country_id: 218},
        {province_id: 4, province_code: "13", province_name: "ปทุมธานี ", country_id: 218},
);*/
    return response()->json($result);
});

Route::get('/getAmphur/{province_id}', function($province_id){

    $result = DB::table('ar_amphur')
    ->join('ar_district', 'ar_amphur.amphur_id', 'ar_district.amphur_id')
    ->where('ar_amphur.province_id',$province_id)
    ->groupBy('ar_amphur.amphur_id')
    ->select('ar_amphur.amphur_id','ar_amphur.amphur_name')
    ->get();

    return response()->json($result);
});

Route::get('/getDistrict/{amphur_id}', function ($amphur_id) {
    $result = DB::table('ar_district')->where('amphur_id', $amphur_id)->get();
    return response()->json($result);
});

Route::get('/getZipcode/{district_id}', function ($district_id) {
    $result = DB::table('ar_zipcode')->where('district_id', $district_id)->first();
    return response()->json($result);
});


Route::get('/getDataAddress/{type}/{id}', function ($type,$id) {

    if($type=="province"){
        $result = DB::table('ar_province')->where('province_id', $id)->first();
    }else if($type=="amphur"){
        $result = DB::table('ar_amphur')->where('amphur_id', $id)->first();
    }else if($type=="district"){
        $result = DB::table('ar_district')->where('district_id', $id)->first();
    }
   
    return response()->json($result);
});